import os
import json
import shutil
import urllib.request


'''
get all directories with the file config.json
'''
def get_directories(fileName: str = "config.json"):
    result = []

    for root, dirs, files in os.walk("."):
        if fileName in files:
            result.append(root)

    return result

'''
read config file of directory
'''
def read_config_file(directory: str, fileName: str = "config.json"):

    filePath = os.path.join(directory, fileName)

    try:
        with open(filePath, "r") as config:
            config_data = json.load(config)

        return config_data

    except FileNotFoundError:
        print(f"File '{filePath}' not found.")
        return None
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON in '{filePath}': {e}")
        return None


'''
create folder based on directory and fodername
'''
def create_folder(directory, name):
    path = os.path.join(directory, name)
    os.mkdir(path)
    return path

'''
copy files
'''
def copy_files(srcPath, dstPath):
    try:
        shutil.copy(srcPath, dstPath)
    except Exception as e:
        print(f"Error: {e}")


'''
download file based on url to path
'''
def download_file(url: str, path: str):
    try:
        with urllib.request.urlopen(url) as response, open(path, "wb+") as file:
            file_size = int(response.info().get('Content-Length', -1))
            chunk_size = 8192
            downloaded_size = 0

            print(f"Downloading: {url}")

            while True:
                chunk = response.read(chunk_size)
                if not chunk:
                    break
                file.write(chunk)
                downloaded_size += len(chunk)

                # Calculate progress
                progress = min(1.0, downloaded_size / file_size)
                progress_percent = progress * 100

                # Print progress bar
                progress_bar_length = 50
                filled_length = int(progress_bar_length * progress)
                bar = '#' * filled_length + '-' * (progress_bar_length - filled_length)

                print(f"\rProgress: [{bar}] {progress_percent:.2f}% - File size: {file_size / 1024 / 1024 / 1024:.2f} GiB", end='', flush=True)

            print("\nDownload complete.")


    except Exception as e:
        print(f"Error downloading file: {e}")



def main():

    # get list of all folders in directory
    dirs = get_directories()

    # iterate folders
    for dir in dirs:

        print(f"{dir}: Run folder")

        # read config data
        print(f"{dir}: Read config")
        currentDir = os.path.join(os.getcwd(), dir)
        config = read_config_file(directory=currentDir)

        # create main-model folder
        print(f"{dir}: Create main-model folder - {config['folderName']}")
        modelFolderPath = create_folder(os.getcwd(), config["folderName"])

        # create input folder
        print(f"{dir}: Create input folder")
        create_folder(modelFolderPath, "input")

        # create output folder
        print(f"{dir}: Create output folder")
        create_folder(modelFolderPath, "output")

        # copy job.sh
        print(f"{dir}: Copy job.sh")
        srcPath = os.path.join(currentDir, "job.sh")
        dstPath = os.path.join(modelFolderPath, "job.sh")
        copy_files(srcPath, dstPath)

        # create model folder
        print(f"{dir}: Create model folder")
        genModelFolderPath = create_folder(modelFolderPath, "model")

        # iterate model folder content - mfc
        for mfc in config["modelFolderContent"]:

            print(f"{dir}: Download file - {mfc['fileName']}")

            # check type
            if(mfc["type"] == "local"):
                # copy file
                srcPath = os.path.join(currentDir, mfc["fileName"])
                dstPath = os.path.join(genModelFolderPath, mfc["fileName"])
                copy_files(srcPath, dstPath)
            elif(mfc["type"] == "extern"):
                dstPath = os.path.join(genModelFolderPath, mfc["fileName"])

                parts = dstPath.split("/")
                parts[1] = mfc["server"]
                dstPath = "/".join(parts)

                url = mfc["url"]

                download_file(url, dstPath)
            elif(mfc["type"] == "extern_folder"):
                dstPath = os.path.join(genModelFolderPath, mfc["fileName"], mfc["folderName"])

                parts = dstPath.split("/")
                parts[1] = mfc["server"]
                dstPath = "/".join(parts)

                url = mfc["url"]

                download_file(url, dstPath)
            else:
                raise Exception(f"unknown type {mfc['type']}")

if __name__ == "__main__":
    main()