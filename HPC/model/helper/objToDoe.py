import pymeshlab as ml

def convert_obj_to_dae(obj_path, dae_path):
    ms = ml.MeshSet()
    ms.load_new_mesh(obj_path)
    ms.save_current_mesh(dae_path)

if __name__ == "__main__":
    import sys

    args = sys.argv

    if(len(args) < 3):
        print("Missing arguments - can not convert .obj to .dae")
    else:

        obj_path = sys.argv[1]
        dae_path = sys.argv[2]

        print(f"Passed obj-path: {obj_path}")
        print(f"Passed dae-path: {dae_path}")

        convert_obj_to_dae(obj_path, dae_path)
