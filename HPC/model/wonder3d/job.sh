#!/bin/bash -l
#                                  # -l is necessary to initialize modules correctly!
#SBATCH --time=00:10:00            # comments start with # and do not count as interruptions
#SBATCH --gres=gpu:a100:1
#SBATCH --partition=a100
#SBATCH --export=NONE              # do not export environment from submitting shell
                                   # first non-empty non-comment line ends SBATCH options
unset SLURM_EXPORT_ENV             # enable export of environment from this script to srun

CKPTS="./model"
FILES="./"

# prepare output paths
mkdir -p $FILES/output/$1
# for instant-nsr-pl
mkdir -p $FILES/output/$1/runs
mkdir -p $FILES/output/$1/exp
# for rembg preprocess step, to save the new input file into
mkdir -p $FILES/output/$1/in

# run models
singularity run --bind $CKPTS/sam_vit_h_4b8939.pth:/opt/Wonder3D/sam_pt/sam_vit_h_4b8939.pth \
                --bind $FILES/output/$1/in:/opt/Wonder3D/p23d/input/ \
                --bind $FILES/input/$1:/opt/Wonder3D/p23d/input/$1 \
                --bind $FILES/output/$1:/opt/Wonder3D/p23d/output \
                --bind $FILES/output/$1/exp:/opt/Wonder3D/instant-nsr-pl/exp/ \
                --bind $FILES/output/$1/runs:/opt/Wonder3D/instant-nsr-pl/runs/ \
                model/wonder3d.sif \
                ./generate.sh $1

# move output file
mv $FILES/output/$1/exp/input/*/save/*.obj $FILES/output/$1/mesh.obj

folderName_without_extension=$(basename "$1" .png)

# rename folder
CWD=$(pwd)
mv $CWD/output/$1 $CWD/output/$folderName_without_extension
