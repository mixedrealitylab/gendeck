#!/bin/bash -l
#                                  # -l is necessary to initialize modules correctly!
#SBATCH --time=00:05:00            # comments start with # and do not count as interruptions
#SBATCH --gres=gpu:a40:1
#SBATCH --partition=a40
#SBATCH --export=NONE              # do not export environment from submitting shell
                                   # first non-empty non-comment line ends SBATCH options
unset SLURM_EXPORT_ENV             # enable export of environment from this script to srun

singularity run --bind model/zero123-xl.ckpt:/opt/one2345/zero123-xl.ckpt \
                    --bind model/sam_vit_h_4b8939.pth:/opt/one2345/sam_vit_h_4b8939.pth \
                    --bind model/ckpt_215000.pth:/opt/one2345/reconstruction/exp/lod0/checkpoints/ckpt_215000.pth \
                    --bind model/indoor_ds_new.ckpt:/opt/one2345/elevation_estimate/utils/weights/indoor_ds_new.ckpt \
                    --bind output:/opt/one2345/exp/ \
                    --bind input/$1:/opt/one2345/$1 \
                    model/one2345.sif \
                    python run.py --img_path $1 --half_precision --output_format .obj
