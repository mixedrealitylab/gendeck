using System.IO;
using UnityEditor;
using UnityEngine;

public static class FileSaver
{
    public static void SaveDAE(string data, string fileName = "model", string folder = "")
    {
        const string FILE_EXTENSION = ".dae";

        // add file extension
        if (!fileName.EndsWith(FILE_EXTENSION))
            fileName += FILE_EXTENSION;

        // default folder main Assets
        if (folder == "")
            folder = "Assets";

        // define file path
        string filePath = Path.Combine(folder, fileName);

        // save file in filesystem
        System.IO.File.WriteAllText(filePath, data);

        AssetDatabase.Refresh();

    }

    public static GameObject LoadDAEFile(string filename)
    {
        AssetDatabase.Refresh();
        string[] files = AssetDatabase.FindAssets(filename);
        if (files.Length > 0)
        {
            string filePath = AssetDatabase.GUIDToAssetPath(files[0]);
            GameObject daeObject = AssetDatabase.LoadAssetAtPath<GameObject>(filePath);
            if (daeObject != null)
            {
                GameObject instance = PrefabUtility.InstantiatePrefab(daeObject) as GameObject;
                return instance;
            }
            else
            {
                Debug.LogError("Failed to load DAE file at path: " + filePath);
            }
        }
        else
        {
            Debug.LogError("DAE file not found: " + filename);
        }
        return null;
    }
}
