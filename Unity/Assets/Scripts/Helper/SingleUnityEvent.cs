using UnityEngine;
using UnityEngine.Events;

public class SingleUnityEvent<T> : UnityEvent<T>
{
    private bool hasListener = false;

    public new void AddListener(UnityAction<T> call)
    {
        if (!hasListener)
        {
            base.AddListener(call);
            hasListener = true;
        }
        else
        {
            Debug.LogWarning("Can not add listener, only one listener is allowed");
        }
    }

    public new void RemoveListener(UnityAction<T> call)
    {
        base.RemoveListener(call);
        hasListener = false;
    }
}
