using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjModifier : MonoBehaviour
{
    //public static GameObject Scale(GameObject gameObjectToScale, GameObject baseGameObject)
    //{
    //    Bounds objectBounds = baseGameObject.GetComponent<Renderer>().bounds;
    //    Bounds meshBounds = gameObjectToScale.GetComponent<Renderer>().bounds;

    //    // Determine the scale factor
    //     Vector3 scaleFactor = new Vector3(
    //         objectBounds.size.x / meshBounds.size.x,
    //         objectBounds.size.y / meshBounds.size.y,
    //         objectBounds.size.z / meshBounds.size.z);

    //   /* Vector3 scaleFactor = new Vector3(0.3f, 0.3f, 0.3f);*/
    //    // Apply the scale factor to the mesh
    //    gameObjectToScale.transform.localScale = scaleFactor;

    //    return gameObjectToScale;
    //}
    //public static GameObject Scale(GameObject gameObjectToScale, GameObject baseGameObject)
    //{
    //    Bounds objectBounds = baseGameObject.GetComponent<Renderer>().bounds;
    //    Bounds meshBounds = gameObjectToScale.GetComponent<Renderer>().bounds;

    //    // Determine the scale factor
    //   /* Vector3 scaleFactor = new Vector3(
    //        objectBounds.size.x / meshBounds.size.x,
    //        objectBounds.size.y / meshBounds.size.y,
    //        objectBounds.size.z / meshBounds.size.z);*/

    //     Vector3 scaleFactor = new Vector3(0.2f, 0.2f, 0.2f);
    //    // Apply the scale factor to the mesh
    //    gameObjectToScale.transform.localScale = scaleFactor;

    //    return gameObjectToScale;
    //}

    public static GameObject Scale(GameObject gameObjectToScale)
    {
        
        Bounds meshBounds = gameObjectToScale.GetComponent<Renderer>().bounds;

        // Determine the scale factor
        /* Vector3 scaleFactor = new Vector3(
             objectBounds.size.x / meshBounds.size.x,
             objectBounds.size.y / meshBounds.size.y,
             objectBounds.size.z / meshBounds.size.z);*/

        Vector3 scaleFactor = new Vector3(0.2f, 0.2f, 0.2f);
        // Apply the scale factor to the mesh
        gameObjectToScale.transform.localScale = scaleFactor;

        return gameObjectToScale;
    }

    public static GameObject CreateWrapperGo(GameObject go, string name)
    {
        GameObject wrapper = new GameObject(name);
        go.transform.parent = wrapper.transform;

        return wrapper;
    }

    public static GameObject SetDefaultSettings(GameObject go)
    {
        go.transform.position = new Vector3(0, 0, 0);
        go.transform.rotation = Quaternion.identity;
        go.transform.localScale = new Vector3(1, 1, 1);

        return go;
    }

    public static void ChildrenToPositionZero(GameObject parent) 
    {
        Transform parentTransform = parent.transform;

        for (int i = 0; i < parentTransform.childCount; i++)
        {
            Transform childTransform = parentTransform.GetChild(i);

            childTransform.localPosition = Vector3.zero;
        }
    }

    public static Vector3 PositionOnTop(GameObject baseObject, GameObject objectToMove)
    {
        // Get the Transform component of both GameObjects
        Transform baseTransform = baseObject.transform;

        // Set the position of the objectToMove to the baseObject's position
        objectToMove.transform.position = baseTransform.position;

        // Move the objectToMove above the baseObject by adding to the y-coordinate
        return baseObject.transform.position + new Vector3(0f, baseObject.transform.localScale.y / 2 + objectToMove.transform.localScale.y / 4, 0f);
    }
}
