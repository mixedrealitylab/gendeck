using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

public class ObjParser
{
    public static GameObject ParseStringToGameObject(string textObj, string id)
    {
        GameObject obj = new GameObject("ParsedMesh");

        string path = Application.dataPath + "/API_Data/1_Models";

        FileSaver.SaveDAE(textObj, fileName: id, folder: path);
        obj = FileSaver.LoadDAEFile(id);

        return obj;
    }
}
