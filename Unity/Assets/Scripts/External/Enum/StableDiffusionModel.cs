using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringValueAttribute : Attribute
{
    public string Value { get; }

    public StringValueAttribute(string value)
    {
        Value = value;
    }
}

public enum StableDiffusionModel
{
    [StringValue("sd_1_5")]
    SD_1_5,
    [StringValue("sd_XL")]
    SX_XL
}

public static class EnumExtensions
{
    public static string GetStringValue(this Enum value)
    {
        var type = value.GetType();
        var fieldInfo = type.GetField(value.ToString());
        var attribute = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
        return attribute.Length > 0 ? attribute[0].Value : null;
    }
}
