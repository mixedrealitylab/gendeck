using System.Collections.Generic;
using UnityEngine;

public enum AzureRegions
{
    francecentral, // Paris, France
    germanywestcentral, // Frankfurt, Germany
    northeurope, // Ireland
    westeurope, // Netherlands
    finlandcentral, // Helsinki, Finland
    swedencentral, // G�vle, Sweden
    switzerlandnorth, // Z�rich, Switzerland
    uksouth, // London, United Kingdom
    ukwest, // Cardiff, United Kingdom
    Eastus
}

