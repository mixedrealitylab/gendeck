using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatusModel3d 
{
    IDLE,
    RUNNING,
    WAIT_FOR_CLEANING,
    FINISHED,
    ERROR
}
