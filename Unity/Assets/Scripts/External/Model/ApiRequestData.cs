using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApiRequestData
{
    public GeneratingModels apiModel { get; set; }
    public string pid { get; set; }
    public string image { get; set; }
    public string model3d { get; set; }
    public StatusModel3d model3d_status { get; set; }
    public string access_token { get; set; }
    public string generatingTime { get; set; }
    public string downloadTime { get; set; }
    public List<Texture2D> downloadedImages { get; set; }
    public bool saveImages { get; set; }
}
