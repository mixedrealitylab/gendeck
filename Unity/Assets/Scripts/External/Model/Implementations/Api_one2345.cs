using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Api_one2345 : ApiRequestModel
{
    public const GeneratingModels MODEL = GeneratingModels.one2345;
    public const float EST_CALC_TIME_3D_MODEL = 5;
    public const float EST_DOWNLOAD_TIME_3D_MODEL = 2;

    public override IEnumerator ManageGeneration(string prompt, ApiEndpointInformation info)
    {
        // -------------------- Init ---------------------------
        // create for new session request data object
        ApiRequestData tempData = this.InitRequestData(MODEL);

        Guid id = Guid.NewGuid();
        string uuid = id.ToString();

        ApiRequestModel.apiRequests[uuid] = tempData;

        // ----------------- Get api token ---------------------
        WWWForm authForm = new WWWForm();
        authForm.AddField("username", info.username);
        authForm.AddField("password", info.password);
        yield return SendPostRequest(
            url: this.CombineUrlPath(info.apiURL, info.loginPath),
            data: authForm,
            token: null,
            identification: uuid,
            onSuccess: OnGetToken,
            onError: OnErrorHandle);

        // ----------------- Upload prompt ---------------------
        WWWForm promptForm = new WWWForm();
        promptForm.AddField("prompt", prompt);

        yield return SendPostRequest(
            url: this.CombineUrlPath(info.apiURL, "model",  MODEL.ToString(), info.promptUploadPath),
            data: promptForm, 
            token: apiRequests[uuid].access_token, 
            identification: uuid, 
            onSuccess: OnUploadPrompt, 
            onError: OnErrorHandle);

        string currentPid = apiRequests[uuid].pid;
        WWWForm pidForm = new WWWForm();
        pidForm.AddField("pid", currentPid);
        pidForm.AddField("useHpc", info.localHPCImageGeneration.ToString());
        pidForm.AddField("imageCount", info.generationImageCount.ToString());
        pidForm.AddField("sdModelName", info.textToImageModel.GetStringValue());
        pidForm.AddField("useImageNr", info.preselectedImageNumber.ToString());

        // ---------------- Create image ----------------------
        yield return SendPostRequest(
            url: this.CombineUrlPath(info.apiURL, "model",  MODEL.ToString(), info.generateImagePath),
            data: pidForm,
            token: apiRequests[uuid].access_token,
            identification: uuid,
            onSuccess: OnGenerateImage,
            onError: OnErrorHandle);


        currentPid = apiRequests[uuid].pid;
        WWWForm getImageForm = new WWWForm();
        pidForm.AddField("pid", currentPid);
        pidForm.AddField("getImageNr", -1);

        // ---------------- get images ----------------------
        yield return SendPostRequest(
            url: this.CombineUrlPath(info.apiURL, "model", MODEL.ToString(), info.getGeneratedImagePath),
            data: pidForm,
            token: apiRequests[uuid].access_token,
            identification: uuid,
            onSuccess: OnGetGenerateImage,
            onError: OnErrorHandle);

        // --------------- Create 3D model --------------------
        yield return SendPostRequest(
            url: this.CombineUrlPath(info.apiURL, "model",  MODEL.ToString(), info.generateModelPath),
            data: pidForm,
            token: apiRequests[uuid].access_token,
            identification: uuid,
            onSuccess: OnGenerateModel,
            onError: OnErrorHandle);

        // ------------ Check 3D model status ------------------
        DebugTextManager.UpdateDisplayedText($"Calc. model (max. {EST_CALC_TIME_3D_MODEL} minutes).");
        int max_check_status = this.GetMaxStatusCheck(checkSeconds: info.checkDelaySeconds, generateMinutes: EST_CALC_TIME_3D_MODEL);

        for (int i = 0; i < max_check_status; i++)
        {
            yield return new WaitForSeconds(info.checkDelaySeconds);
            yield return SendPostRequest(
                url: this.CombineUrlPath(info.apiURL, "model",  MODEL.ToString(), info.statusPath),
                data: pidForm,
                token: apiRequests[uuid].access_token,
                identification: uuid,
                onSuccess: OnGetStatus,
                onError: OnErrorHandle);

            // check if model is finished
            StatusModel3d currentModelStatus = apiRequests[uuid].model3d_status;
            if (currentModelStatus == StatusModel3d.WAIT_FOR_CLEANING)
                break;
        }

        // --------------- Download 3D model --------------------
        DebugTextManager.UpdateDisplayedText($"Download model (max. {EST_DOWNLOAD_TIME_3D_MODEL} minutes).");
        yield return SendPostRequest(
                url: this.CombineUrlPath(info.apiURL, "model",  MODEL.ToString(), info.downloadModelPath),
                data: pidForm,
                token: apiRequests[uuid].access_token,
                identification: uuid,
                onSuccess: OnDownloadModel,
                onError: OnErrorHandle);

        // --------------- Clean up server --------------------
        yield return SendPostRequest(
                url: this.CombineUrlPath(info.apiURL, "model", MODEL.ToString(), info.cleanPath),
                data: pidForm,
                token: apiRequests[uuid].access_token,
                identification: uuid,
                onSuccess: OnCleanUp,
                onError: OnErrorHandle);

        // --------- Delete process on server ------------------
        yield return SendPostRequest(
                url: this.CombineUrlPath(info.apiURL, "model",  MODEL.ToString(), info.deleteRecordPath),
                data: pidForm,
                token: apiRequests[uuid].access_token,
                identification: uuid,
                onSuccess: OnDeleteProcess,
                onError: OnErrorHandle);

        // ------------------- Display model -------------------
        this.VisualizationModel3d(apiRequests[uuid]);

        // ------------------- Finish --------------------------
        apiRequests.Remove(uuid);
        DebugTextManager.UpdateDisplayedText($"Finished.");
    }

    public override void VisualizationModel3d(ApiRequestData data)
    {
        GameObject obj = Model3dPlacing.PlaceRequestResults(data);
    }
}
