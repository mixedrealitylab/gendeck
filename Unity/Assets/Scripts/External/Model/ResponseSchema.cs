using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResponseSchema
{
    [System.Serializable]
    public class TokenData
    {
        public string access_token;
        public string token_type;
    }

    [System.Serializable]
    public class PidData
    {
        public string pid;
    }

    [System.Serializable]
    public class StatusData
    {
        public string status;
        public string startTime;
    }

    [System.Serializable]
    public class DownloadData
    {
        public string status;
        public string content;
    }

    [System.Serializable]
    public class ImageListData
    {
        public List<string> imagesBase64;
    }

}
