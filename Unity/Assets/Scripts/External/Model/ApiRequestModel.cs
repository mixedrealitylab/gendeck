using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
# if UNITY_EDITOR
using UnityEditor;
# endif
using UnityEngine;
using UnityEngine.Networking;
using static ResponseSchema;

public abstract class ApiRequestModel : MonoBehaviour 
{
    private GameObject audioObject1;
   
    #region data
    protected static Dictionary<string, ApiRequestData> apiRequests = new Dictionary<string, ApiRequestData>();
    #endregion

    #region abstracts
    public abstract IEnumerator ManageGeneration(string prompt, ApiEndpointInformation info);
    public abstract void VisualizationModel3d(ApiRequestData data);
    #endregion

    #region functions
    protected ApiRequestData InitRequestData(GeneratingModels model)
    {
        ApiRequestData temp = new ApiRequestData();
        temp.apiModel = model;

        return temp;
    }

    protected IEnumerator SendPostRequest(
        string url, WWWForm data, string token,
        string identification,
        Action<DownloadHandler, string> onSuccess, 
        Action<string, string> onError
        )
    {
        // validate data
        if (data == null) { data = new WWWForm(); }

        // send post request to url
        using (UnityWebRequest request = UnityWebRequest.Post(url, data))
        {
            // add authentification token
            if (token != null)
                request.SetRequestHeader("Authorization", "Bearer " + token);

            // send request
            yield return request.SendWebRequest();

            // check request result
            if (request.result == UnityWebRequest.Result.Success)
                onSuccess(request.downloadHandler, identification);
            else
            {
                Debug.LogError($"URL: {url}; Data: {data}; ERROR: {request.error}");
                onError(request.error, identification);
            }
                
        }
    }

    protected string CombineUrlPath(params string[] parameter)
    {
        if (parameter == null)
        {
            throw new ArgumentNullException(nameof(parameter));
        }

        return string.Join("/", parameter);
    }
    #endregion

    #region API functions
    protected void OnGetToken(DownloadHandler result, string identificator)
    {
        if(ApiRequestModel.apiRequests.ContainsKey(identificator))
        {
            ResponseSchema.TokenData tokenData = JsonUtility.FromJson<ResponseSchema.TokenData>(result.text);
            ApiRequestModel.apiRequests[identificator].access_token = tokenData.access_token;
        }
        else
            DebugTextManager.UpdateDisplayedText($"Do not find identificator: {identificator}");
    }

    protected void OnUploadPrompt(DownloadHandler result, string identificator)
    {
        if (ApiRequestModel.apiRequests.ContainsKey(identificator))
        {
            ResponseSchema.PidData tokenData = JsonUtility.FromJson<ResponseSchema.PidData>(result.text);
            ApiRequestModel.apiRequests[identificator].pid = tokenData.pid;

            DebugTextManager.UpdateDisplayedText($"Uplouded prompt");
        }
        else
            DebugTextManager.UpdateDisplayedText($"Do not find identificator: {identificator}"); 
    }

    protected void OnGenerateImage(DownloadHandler result, string identificator)
    {
        if (ApiRequestModel.apiRequests.ContainsKey(identificator))
            DebugTextManager.UpdateDisplayedText($"Generate image");
        else
            DebugTextManager.UpdateDisplayedText($"Do not find identificator: {identificator}");
    }

    protected void OnGetGenerateImage(DownloadHandler result, string identificator)
    {
        if (ApiRequestModel.apiRequests.ContainsKey(identificator))
            DebugTextManager.UpdateDisplayedText($"Get Generate image");
        else
            DebugTextManager.UpdateDisplayedText($"Do not find identificator: {identificator}");


        // If the request was successful
        if (result != null && result.text != "")
        {
            // Deserialize JSON response to ImageListData object
            ImageListData responseData = JsonUtility.FromJson<ImageListData>(result.text);

#if UNITY_EDITOR
            // Create the folder if it doesn't exist
            string folderPath = "Assets/API_Data/0_Textures";
            if (!AssetDatabase.IsValidFolder(folderPath))
            {
                string parentFolder = "Assets/API_Data";
                AssetDatabase.CreateFolder(parentFolder, "0_Textures");
            }
# endif

            // Check if the response data is not null and contains images
            if (responseData != null && responseData.imagesBase64 != null && responseData.imagesBase64.Count > 0)
            {
                // Load images from base64 strings
                List<Texture2D> textures = new List<Texture2D>();

                // Load images from base64 strings
                for (int i = 0; i < responseData.imagesBase64.Count; i++)
                {
                    // convert response to texture
                    string base64Img = responseData.imagesBase64[i];
                    byte[] imgBytes = Convert.FromBase64String(base64Img);
                    Texture2D texture = new Texture2D(5, 5);
                    texture.LoadImage(imgBytes);

                    // add to list
                    textures.Add(texture);

                    // save data
                    bool save = ApiRequestModel.apiRequests[identificator].saveImages;

                    if (save)
                    {
#if UNITY_EDITOR
                        // Save the texture as an a png file
                        string filePath = $"{folderPath}/{identificator}_{i}.png";

                        byte[] bytes = texture.EncodeToPNG();
                        File.WriteAllBytes(filePath, bytes);
#endif
                    }


                }

                ApiRequestModel.apiRequests[identificator].downloadedImages = textures;

#if UNITY_EDITOR
                AssetDatabase.Refresh();
# endif
            }
            else
            {
                Debug.LogError("Error loading images: Invalid response data or empty response");
            }
        }
        else
        {
            Debug.LogError("Error loading images: Received empty response");
        }
    }

    protected void OnGenerateModel(DownloadHandler result, string identificator)
    {
        if (ApiRequestModel.apiRequests.ContainsKey(identificator))
        {
            GeneratingModels model = ApiRequestModel.apiRequests[identificator].apiModel;
            DebugTextManager.UpdateDisplayedText($"Generate model {model}");
        }
        else
            DebugTextManager.UpdateDisplayedText($"Do not find identificator: {identificator}");
    }

    protected void OnDownloadModel(DownloadHandler result, string identificator)
    {
        if (ApiRequestModel.apiRequests.ContainsKey(identificator))
        {
            ResponseSchema.DownloadData downloadData = JsonUtility.FromJson<ResponseSchema.DownloadData>(result.text);
            ApiRequestModel.apiRequests[identificator].model3d = downloadData.content;
        }
        else
            DebugTextManager.UpdateDisplayedText($"Do not find identificator: {identificator}");
    }

    protected void OnGetStatus(DownloadHandler result, string identificator)
    {
        if (ApiRequestModel.apiRequests.ContainsKey(identificator))
        {
            ResponseSchema.StatusData statusData = JsonUtility.FromJson<ResponseSchema.StatusData>(result.text);
            this.DefineModelStatus(statusData.status, identificator);
        }
        else
            DebugTextManager.UpdateDisplayedText($"Do not find identificator: {identificator}");
    }

    private void DefineModelStatus(string status, string identificator)
    {
        if (status == "IDLE")
            ApiRequestModel.apiRequests[identificator].model3d_status = StatusModel3d.IDLE;
        else if (status == "RUNNING")
            ApiRequestModel.apiRequests[identificator].model3d_status = StatusModel3d.RUNNING;
        else if (status == "WAIT_FOR_CLEANING")
            ApiRequestModel.apiRequests[identificator].model3d_status = StatusModel3d.WAIT_FOR_CLEANING;
        else if (status == "FINISHED")
            ApiRequestModel.apiRequests[identificator].model3d_status = StatusModel3d.FINISHED;
        else
            ApiRequestModel.apiRequests[identificator].model3d_status = StatusModel3d.FINISHED;
    }

    protected void OnCleanUp(DownloadHandler result, string identificator)
    {
        DebugTextManager.UpdateDisplayedText($"Clean up");
    }

    protected void OnDeleteProcess(DownloadHandler result, string identificator)
    {
        //DebugTextManager.UpdateDisplayedText($"Delete {identificator}");
        audioObject1 = GameObject.Find("--- Audio ---");
        AudioSource audioSource1 = audioObject1.GetComponent<AudioSource>();
        audioSource1.Play();
        ObjectStatusPrompt.DisablingGenerationPrompt();

    }

    protected void OnErrorHandle(string error, string identificator)
    {
        DebugTextManager.UpdateDisplayedText($"Stop API interaction of {identificator}: {error}");
        throw new Exception($"Stop API interaction of {identificator}: {error}");
    }

    protected int GetMaxStatusCheck(int checkSeconds, float generateMinutes, float pufferPercentage = 0.25f)
    {
        float countChecksForGen = ((generateMinutes * 60) / checkSeconds);

        float countChecksForGenBuffer = countChecksForGen * pufferPercentage;

        float result = countChecksForGen + countChecksForGenBuffer;

        return (int)result;
    }
    #endregion

}
