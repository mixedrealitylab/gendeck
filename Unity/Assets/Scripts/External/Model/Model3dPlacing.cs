using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public static class Model3dPlacing
{
    public static Material objMaterial = null;
    public static GameObject parentObj = null;
    public static GameObject spawnObj = null;

    public static string savePath;
    public static MeshFilter meshFilter;
    public static GameObject PlaceRequestResults(ApiRequestData data)
    {
        // get gameobject for string
        GameObject parsedGo = ObjParser.ParseStringToGameObject(data.model3d, data.pid);

        MeshFilter tempMeshFilter = parsedGo.GetComponent<MeshFilter>();

        // set material
        MeshRenderer meshRenderer = parsedGo.GetComponent<MeshRenderer>();
        if (meshRenderer != null)
            meshRenderer.material = Model3dPlacing.objMaterial;

        // set default elements - position/direciton/scale
        parsedGo = ObjModifier.SetDefaultSettings(parsedGo);

        // create wrapper around
        GameObject wrapper = ObjModifier.CreateWrapperGo(parsedGo, data.pid);

        // placing in hierachy
        wrapper.transform.parent = Model3dPlacing.parentObj.transform;

        // add VR interactions
        parsedGo = VR_Interaction.SetUpMeshGameObject(parsedGo);
        parsedGo = VR_Interaction.AddGrabInteractible(parsedGo);
        parsedGo = VR_Interaction.AddHandGrabInteractable(parsedGo);

        // scaling
        //ObjModifier.Scale(parsedGo, scaleObject);
        ObjModifier.Scale(parsedGo);

        // correct general position
        ObjModifier.ChildrenToPositionZero(wrapper);

        // correct position of mesh
        Vector3 position = ObjModifier.PositionOnTop(baseObject: Model3dPlacing.spawnObj, objectToMove: wrapper);
        wrapper.transform.position = position;

        return wrapper;
    }
}
