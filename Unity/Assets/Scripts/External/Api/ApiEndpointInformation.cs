using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ApiEndpointInformation : MonoBehaviour
{
    #region inputs

    [Header("API Information")]
    [SerializeField] public string apiURL = "http://127.0.0.1:8000";

    [Header("API Authentification")]
    [SerializeField] public string username = "user";
    [SerializeField] public string password = "password";

    [Header("API Endpoints")]
    [SerializeField] public string loginPath;
    [SerializeField] public string promptUploadPath;
    [SerializeField] public string generateImagePath;
    [SerializeField] public string getGeneratedImagePath;
    [SerializeField] public string generateModelPath;
    [SerializeField] public string downloadModelPath;
    [SerializeField] public string statusPath;
    [SerializeField] public string cleanPath;
    [SerializeField] public string deleteRecordPath;

    [Header("API Status")]
    [SerializeField] public int checkDelaySeconds = 2;

    [Header("API Image Generation")]
    [SerializeField] public bool localHPCImageGeneration = false;
    [SerializeField] public uint generationImageCount = 1;
    [SerializeField] public StableDiffusionModel textToImageModel = StableDiffusionModel.SD_1_5;
    [SerializeField] public uint preselectedImageNumber = 1;

    [Header("API Data saving")]
    [SerializeField] public bool saveGeneratedImages = false;

    #endregion


    void Start()
    {
        // Check if defined apiURL is a valid url
        if (!IsValidURL(apiURL)) 
            throw new System.Exception("Defined API-URL is not vaild");
        
    }

    private bool IsValidURL(string url)
    {
        // Regular expression pattern to validate URL
        string pattern = @"^(https?)://([\w-]+(\.[\w-]+)+|([a-zA-Z]{1}|[\w-]{2,}))(:[0-9]{1,5})?((/?)|(/([\w#!:.?+=&%@!\-\/]))?)$";

        // Create a regex object
        Regex regex = new Regex(pattern);

        // Check if the URL matches the pattern
        return regex.IsMatch(url);
    }

    public void HpcIsSelected()
    {
        // use HPC to generate text to image
        localHPCImageGeneration = true;
        DebugTextManager.UpdateDisplayedText($"Use HPC for TextToImage");
    }

    public void HuggingFaceIsSelected() 
    { 
        // use Hugging Face to generate text to image
        localHPCImageGeneration = false;
        DebugTextManager.UpdateDisplayedText($"Use Hugging Face for TextToImage");
    }

}
