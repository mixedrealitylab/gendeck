using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ApiEndpointInformation))]
public class ApiManager : MonoBehaviour
{
    #region Components
    static ApiEndpointInformation apiEndpointInformation;
    private static ApiManager instance;
    #endregion

    #region inspector variables
    [Header("API Model Information")]
    [SerializeField] private GeneratingModels _model = GeneratingModels.demo;

    [Space(10)]
    [Header("SST Text")]
    [SerializeField] private string preText = "";
    [SerializeField] private string postText = "";

    [Space(10)]
    [Header("Object placing")]
    [SerializeField] private GameObject parentHierarchy;
    [SerializeField] private GameObject spawnObject;
    [SerializeField] private Material objMaterial;

    #endregion

    #region variables
    public static GeneratingModels model;
    #endregion

    #region Validate load and change
    private void OnValidate()
    {
        apiEndpointInformation = GetComponent<ApiEndpointInformation>();

        if (apiEndpointInformation == null) 
            throw new System.Exception("Component ApiEndpointInformation is not defined");
    }
    #endregion

    // Start is called before the first frame update
    void Awake()
    {
        ApiManager.model = _model;

        instance = this;
    }

    void Start()
    {
        DebugTextManager.UpdateDisplayedText($"Current model: {ApiManager.model}");
    }

    public static void UpdateModel(GeneratingModels newModel)
    {
        if (ApiManager.model == newModel) return;

        ApiManager.model = newModel;
        DebugTextManager.UpdateDisplayedText($"Change model to {ApiManager.model}");
    }

    public static void RunSelectedModelRequest(string startInfo = "")
    {
        if (startInfo != "")
            DebugTextManager.UpdateDisplayedText(startInfo);

        // define placing information
        Model3dPlacing.objMaterial = instance.objMaterial;
        Model3dPlacing.parentObj = instance.parentHierarchy;
        Model3dPlacing.spawnObj = instance.spawnObject;
        ApiRequestModel apiModel = null;

        // get current user text
        string currentText = SpeechToText_ServiceManager.GetLastRecognizedText();

        // add pre text and post text
        string promptText = "";

        if(instance.preText != "")
        {
            promptText = instance.preText + " " + currentText;
        } 

        if(instance.postText != "")
        {
            promptText = promptText + " " + instance.postText;
        }

        switch (ApiManager.model)
        {
            case GeneratingModels.demo:
                apiModel = new Api_one2345();
                instance.StartCoroutine(apiModel.ManageGeneration(currentText, ApiManager.apiEndpointInformation));
                break;
            case GeneratingModels.wonder3d:
                apiModel = new Api_wonder3d();
                instance.StartCoroutine(apiModel.ManageGeneration(currentText, ApiManager.apiEndpointInformation));
                break;
            case GeneratingModels.one2345:
                apiModel = new Api_one2345();
                instance.StartCoroutine(apiModel.ManageGeneration(currentText, ApiManager.apiEndpointInformation));
                break;
            case GeneratingModels.eShapeTextTo3d:
                apiModel = new Api_eShapeTextToModel();
                instance.StartCoroutine(apiModel.ManageGeneration(currentText, ApiManager.apiEndpointInformation));
                break;
            default:
                DebugTextManager.UpdateDisplayedText($"Invalid model selection: {ApiManager.model}");
                break;
        }
       
    }
}
