using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SpeechToText_ServiceManager))]
public class SpeechServiceButtonLogic : MonoBehaviour
{
    #region event buttons
    public static UnityEvent<string> ConfirmIsPressed = new UnityEvent<string>();
    public static UnityEvent PlayIsPressed = new UnityEvent();
    public static UnityEvent StopIsPressed = new UnityEvent();
    #endregion

    #region components
    SpeechToText_ServiceManager stt_manager;
    #endregion

    #region variables
    private bool recording = false;
    private bool genFinished = false;
    #endregion

    void OnValidate()
    {
        stt_manager = GetComponent<SpeechToText_ServiceManager>();
    }

    private void Start()
    {
        SpeechTextManager.UpdateDisplayedText("Ready");
    }

    public void PlayButtonIsPressed()
    {
        if (recording == true || genFinished == true)
        {
            DebugTextManager.ResetDisplayedText("Info:");
            genFinished = false;
        }
        else
        {
            recording = true;
        }

        // start recording
        stt_manager.GetSTT().StartContinuousRecognitionAsync();

        // output
        DebugTextManager.UpdateDisplayedText("UserButton: Pressed record");
        SpeechTextManager.UpdateDisplayedText("Recording...");

        PlayIsPressed.Invoke();
    }
    public void StopButtonIsPressed()
    {
        recording = false;

        // stop recording
        stt_manager.GetSTT().StopContinuousRecognitionAsync();

        // output
        DebugTextManager.UpdateDisplayedText("UserButton: Pressed stop");

        StopIsPressed.Invoke();

        if(SpeechTextManager.CurrentText() == "Recording...")
        {
            SpeechTextManager.UpdateDisplayedText("Ready");
        }
    }
    public void ConfirmButtonIsPressed()
    {
        recording = false;

        ConfirmIsPressed.Invoke(SpeechToText_ServiceManager.GetLastRecognizedText());

        // output
        DebugTextManager.UpdateDisplayedText("UserButton: Pressed confirm");

        ApiManager.RunSelectedModelRequest();

        genFinished = true;
    }
}
