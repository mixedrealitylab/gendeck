using UnityEngine;
using Microsoft.CognitiveServices.Speech;
using Microsoft.CognitiveServices.Speech.Audio;
using System;
using System.Collections.Generic;

public enum SpeechLanguage
{
    GERMAN,
    ENGLISH,
}

[RequireComponent(typeof(ApiEndpointInformation))]
public class SpeechToText_ServiceManager : MonoBehaviour
{
    #region variables
    private AudioConfig _audioConfig;
    private SpeechConfig _speechConfig;
    private SpeechRecognizer speechRecognizer;

    private static string recognizedText = "";
    private string lastRecognizedText = "";
    private List<string> debugTextBuffer = new List<string>();
    
    #endregion

    #region inspector
    [SerializeField] private SpeechLanguage speechLanguage = SpeechLanguage.GERMAN;
    [SerializeField] private string apiKey;
    [SerializeField] private AzureRegions region = AzureRegions.westeurope;
    #endregion

    void InitAzureService()
    {
        // authentifcation
        _speechConfig = SpeechConfig.FromSubscription(apiKey, region.ToString().ToLower());

        // additional configiguration
        _speechConfig.SetProperty(PropertyId.SpeechServiceResponse_PostProcessingOption, "TrueText");

        // speech language
        _speechConfig.SpeechRecognitionLanguage = GetLanguageString(speechLanguage);

        // text language
        _speechConfig.SpeechSynthesisLanguage = GetLanguageString(speechLanguage);

        // audio input
        _audioConfig = AudioConfig.FromDefaultMicrophoneInput();

        // define Recognizer object
        speechRecognizer = new SpeechRecognizer(_speechConfig, _audioConfig);


        // Register function to events

        // Session startet -> record
        speechRecognizer.SessionStarted += SessionStarted;

        // Session stopped -> stopped recording
        speechRecognizer.SessionStopped += SessionStopped;

        // Azure recognized text
        speechRecognizer.Recognized += Recognized;

        // Problem with service
        speechRecognizer.Canceled += Cancled;
    }

    void Start()
    {
        TestInternetConnection();

        try
        {
            InitAzureService();
            DebugTextManager.UpdateDisplayedText("Init. Azure SST Service");
        }
        catch(Exception ex)
        {
            DebugTextManager.UpdateDisplayedText($"Error: {ex.Message}");
        }
    }

    private void SessionStarted(object s, SessionEventArgs e)
    {
        lastRecognizedText = "";
        DebugTextManager.UpdateDisplayedText("SST Session is recording");
        SpeechTextManager.UpdateDisplayedText("Recording...");
    }

    private void SessionStopped(object s, SessionEventArgs e)
    {
        lastRecognizedText = "";
        DebugTextManager.UpdateDisplayedText("SST Session is stopped");
    }

    private void Recognized(object s, SpeechRecognitionEventArgs e)
    {
        // function is called not in MainThread!

        if (e.Result.Reason == ResultReason.RecognizedSpeech)
        {
            SpeechToText_ServiceManager.recognizedText = e.Result.Text.Replace(".", "");

            Debug.Log(new string('-', 20));
            Debug.Log($"Recognized:\t{SpeechToText_ServiceManager.recognizedText}");
            Debug.Log($"Duration:\t{e.Result.Duration.TotalSeconds} seconds");
            Debug.Log(new string('-', 20));

            debugTextBuffer.Add($"Recognized-Text: {SpeechToText_ServiceManager.recognizedText}");
        }
    }

    private void Cancled(object s, SpeechRecognitionCanceledEventArgs e)
    {
        lastRecognizedText = "";
        DebugTextManager.UpdateDisplayedText($"CANCELED: Reason={e.Reason} ErrorDetails={e.ErrorDetails}.");
    }

    private void Update()
    {
        // detected change
        if(lastRecognizedText != SpeechToText_ServiceManager.recognizedText)
        {
            SpeechTextManager.UpdateDisplayedText(SpeechToText_ServiceManager.recognizedText);
            lastRecognizedText = SpeechToText_ServiceManager.recognizedText;
        }

        if(debugTextBuffer.Count > 0)
        {
            foreach (string value in debugTextBuffer)
            {
                DebugTextManager.UpdateDisplayedText(value);
            }

            debugTextBuffer.Clear();
        }
        
    }

    void OnDestroy()
    {
        if (speechRecognizer != null)
        {
            speechRecognizer.StopContinuousRecognitionAsync().Wait();
            speechRecognizer.Dispose();
        }
    }

    string GetLanguageString(SpeechLanguage language)
    {
        string selectedLanguage = "";

        switch (language)
        {
            case SpeechLanguage.GERMAN:
                selectedLanguage = "de-DE";
                break;
            default:
                selectedLanguage = "en-US";
                break;
        }

        return selectedLanguage;
    }

    void TestInternetConnection()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
            DebugTextManager.UpdateDisplayedText("Have NO connection.");
        else
            DebugTextManager.UpdateDisplayedText("Have internet connection.");
        
    }

    public SpeechRecognizer GetSTT()
    {
        return this.speechRecognizer;
    }

    public static string GetLastRecognizedText()
    {
        return SpeechToText_ServiceManager.recognizedText;
    }

    public static void SetLastRecognizedText(string text)
    {
        SpeechToText_ServiceManager.recognizedText = text;
    }
}

