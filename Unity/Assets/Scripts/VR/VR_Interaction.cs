using Oculus.Interaction;
using Oculus.Interaction.HandGrab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR_Interaction : MonoBehaviour
{
    public static GameObject AddGrabInteractible(GameObject grabObject)
    {
        GameObject grabInteractableGo = new GameObject("GrabInteractable");

        // add interactable component
        grabInteractableGo.AddComponent<GrabInteractable>();

        // set up component
        GrabInteractable componentInteractable = grabInteractableGo.GetComponent<GrabInteractable>();
        componentInteractable.MaxInteractors = -1;
        componentInteractable.MaxSelectingInteractors = -1;

        Grabbable grabbable = grabObject.GetComponent<Grabbable>();

        if (grabbable != null )
            componentInteractable.InjectOptionalPointableElement(grabbable);
        else
            Debug.LogError("Can not set Pointable element");


        Rigidbody rigid = grabObject.GetComponent<Rigidbody>();

        if (rigid != null)
            componentInteractable.InjectRigidbody(rigid);
        else
            Debug.LogError("Can not set Rigidbody");

        componentInteractable.UseClosestPointAsGrabSource = true;
        componentInteractable.ReleaseDistance = 0;
        componentInteractable.ResetGrabOnGrabsUpdated = true;

        componentInteractable.InjectOptionalGrabSource(grabObject.transform);

        PhysicsGrabbable phyGrab = grabObject.GetComponent<PhysicsGrabbable>();

        if (phyGrab != null)
            componentInteractable.InjectOptionalPhysicsGrabbable(phyGrab);
        else
            Debug.LogError("Can not set PhysicsGrabbable");



        // add in hierachy
        grabInteractableGo.transform.parent = grabObject.transform.parent;

        return grabObject;
    }

    public static GameObject AddHandGrabInteractable(GameObject grabObject)
    {
        GameObject grabInteractableGo = new GameObject("GrabInteractable");

        // add interactable component
        grabInteractableGo.AddComponent<HandGrabInteractable>();
    


        // set up GrabInteractable component


        // set up GrabInteractable component
        HandGrabInteractable componentHandGrabInteractable = grabInteractableGo.GetComponent<HandGrabInteractable>();
        componentHandGrabInteractable.MaxInteractors = -1;
        componentHandGrabInteractable.MaxSelectingInteractors = -1;

        Grabbable grabbable = grabObject.GetComponent<Grabbable>();
        if (grabbable != null)
            componentHandGrabInteractable.InjectOptionalPointableElement(grabbable);
        else
            Debug.LogError("Can not set Pointable element");

        Rigidbody rigid = grabObject.GetComponent<Rigidbody>();
        if (rigid != null)
            componentHandGrabInteractable.InjectRigidbody(rigid);
        else
            Debug.LogError("Can not set Rigidbody");

        componentHandGrabInteractable.ResetGrabOnGrabsUpdated = true;
        componentHandGrabInteractable.InjectSupportedGrabTypes(Oculus.Interaction.Grab.GrabTypeFlags.Pinch);
        
        PhysicsGrabbable phyGrab = grabObject.GetComponent<PhysicsGrabbable>();
        if (phyGrab != null)
            componentHandGrabInteractable.InjectOptionalPhysicsGrabbable(phyGrab);
        else
            Debug.LogError("Can not set PhysicsGrabbable");

        HandGrabInteractable handGrabInteractable = componentHandGrabInteractable.GetComponent<HandGrabInteractable>();
        if (handGrabInteractable != null)
            componentHandGrabInteractable.InjectOptionalData(handGrabInteractable);
        else
            Debug.LogError("Can not set HandGrabInteractable");


        // add in hierachy
        grabInteractableGo.transform.parent = grabObject.transform.parent;

        return grabObject;
    }

    public static GameObject SetUpMeshGameObject(GameObject obj)
    {

        // add components MeshCollider
        MeshCollider collid = obj.GetComponent<MeshCollider>();

        if(collid == null)
        {
            obj.AddComponent<MeshCollider>();
        }

        collid = obj.GetComponent<MeshCollider>();
        if(collid != null)
        {
            collid.convex = true;
        }


        // add components OneGrabFreeTransformer
        obj.AddComponent<Grabbable>();

        Grabbable grabbl = obj.GetComponent<Grabbable>();
        grabbl.TransferOnSecondSelection = false;
        grabbl.AddNewPointsToFront = false;
        grabbl.MaxGrabPoints = -1;
        grabbl.InjectOptionalTargetTransform(obj.transform);


        // add components OneGrabFreeTransformer
        obj.AddComponent<OneGrabFreeTransformer>();

        OneGrabFreeTransformer oneGrab = obj.GetComponent<OneGrabFreeTransformer>();

        // add components TwoGrabFreeTransformer
        obj.AddComponent<TwoGrabFreeTransformer>();

        TwoGrabFreeTransformer twoGrab = obj.GetComponent<TwoGrabFreeTransformer>();
        TwoGrabFreeTransformer.TwoGrabFreeConstraints constraint = new TwoGrabFreeTransformer.TwoGrabFreeConstraints();
        FloatConstraint maxFloatConstraint = new FloatConstraint();
        maxFloatConstraint.Value = 3f;
        FloatConstraint minFloatConstraint = new FloatConstraint();
        minFloatConstraint.Value = 0.1f;

        constraint.MaxScale = maxFloatConstraint;
        constraint.MinScale = minFloatConstraint;

        twoGrab.InjectOptionalConstraints(constraint);

        // add components Rigidbody
        obj.AddComponent<Rigidbody>();

        Rigidbody rigid = obj.GetComponent<Rigidbody>();
        rigid.mass = 0.5f;
        rigid.drag = 0.2f;
        rigid.angularDrag = 0.05f;
        rigid.automaticCenterOfMass = true;
        rigid.automaticInertiaTensor = true;
        rigid.useGravity = false;
        rigid.isKinematic = true;
        rigid.interpolation = RigidbodyInterpolation.None;
        rigid.collisionDetectionMode = CollisionDetectionMode.Discrete;
  

        // add components PhysicsGrabbable
        obj.AddComponent<PhysicsGrabbable>();

        PhysicsGrabbable phyGrib = obj.GetComponent<PhysicsGrabbable>();

        phyGrib.InjectGrabbable(grabbl);
        phyGrib.InjectRigidbody(rigid);
        phyGrib.InjectOptionalScaleMassWithSize(true);


        // add components InteractableTriggerBroadcaster
        obj.AddComponent<InteractableTriggerBroadcaster>();
        obj.AddComponent<InteractableTriggerBroadcaster>();


        grabbl.InjectOptionalOneGrabTransformer(oneGrab);
        grabbl.InjectOptionalTwoGrabTransformer(twoGrab);

        return obj;
    }
}
