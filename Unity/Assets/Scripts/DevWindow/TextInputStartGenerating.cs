using UnityEngine;
using UnityEditor;

public class TextInputStartGenerating : EditorWindow
{
    private string text = "";
    private GeneratingModels genModel;

    [MenuItem("Window/Text Input Start Generating")]
    public static void ShowWindow()
    {
        GetWindow<TextInputStartGenerating>("Text Input Start Generating");
    }

    private void OnGUI()
    {
        GUILayout.Label("Start ShapE without Voice-Input", EditorStyles.boldLabel);

        GUILayout.Space(10f);

        GUILayout.Label("Text");
        text = EditorGUILayout.TextField(text);

        GUILayout.Label("Model");
        genModel = (GeneratingModels)EditorGUILayout.EnumPopup("Model to use: ", genModel);

        GUILayout.Space(10f);

        if (GUILayout.Button("Start"))
        {
            // set text
            SpeechToText_ServiceManager.SetLastRecognizedText(text);

            // set model
            ApiManager.UpdateModel(genModel);

            // start
            ApiManager.RunSelectedModelRequest();

            
        }
    }

    
}
