using UnityEngine;
using UnityEditor;

public class DAEFileLoaderWindow : EditorWindow
{
    private string filename = ""; // Pfad zur .doa-Datei

    [MenuItem("Window/DAE File Loader")]
    public static void ShowWindow()
    {
        GetWindow<DAEFileLoaderWindow>("DAE File Loader");
    }

    private void OnGUI()
    {
        GUILayout.Label("DAE File Loader", EditorStyles.boldLabel);

        GUILayout.Space(10f);

        GUILayout.Label("Filename:");
        filename = EditorGUILayout.TextField(filename);

        GUILayout.Space(10f);

        if (GUILayout.Button("Load DAE File"))
        {
            //GameObject doaObject = FileSaver.LoadDAEFile(filename);
            



        }
    }

    
}
