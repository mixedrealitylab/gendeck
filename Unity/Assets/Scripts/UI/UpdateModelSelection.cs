using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateModelSelection : MonoBehaviour
{
    public void SetWonder3D()
    {
        ApiManager.UpdateModel(GeneratingModels.wonder3d);
        Debug.Log($"Switch model to {GeneratingModels.wonder3d}");
    }
    public void SetOne2345()
    {
        ApiManager.UpdateModel(GeneratingModels.one2345);
        Debug.Log($"Switch model to {GeneratingModels.one2345}");
    }
    public void SetShapeE()
    {
        ApiManager.UpdateModel(GeneratingModels.eShapeTextTo3d);
        Debug.Log($"Switch model to {GeneratingModels.eShapeTextTo3d}");
    }
}
