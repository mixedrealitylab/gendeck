using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectStatusPrompt : MonoBehaviour
{
    private static GameObject objectGenerationPrompt;
    private static GameObject objectGenerationIcon;
    // Start is called before the first frame update
    void Start()
    {
        objectGenerationPrompt = GameObject.Find("IsGeneratingPrompt");
        objectGenerationIcon = GameObject.Find("IsGeneratingIcon");
        objectGenerationPrompt.SetActive(false);
        objectGenerationIcon.SetActive(false);

    }

    // Update is called once per frame
    public static void ShowingGenerationPrompt()
    {
        objectGenerationPrompt.SetActive(true);
        objectGenerationIcon.SetActive(true);
    }
    public static void DisablingGenerationPrompt()
    {
        objectGenerationPrompt.SetActive(false);
        objectGenerationIcon.SetActive(false);
    }
}
