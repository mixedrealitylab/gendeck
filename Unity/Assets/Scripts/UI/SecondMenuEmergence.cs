using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondMenuEmergence : MonoBehaviour
{
    [SerializeField] public GameObject[] secondMenuObjects=new GameObject[3];
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < secondMenuObjects.Length; i++) secondMenuObjects[i].SetActive(false);
    }

   public void ActiveSecondMenue()
    {
        for (int i = 0; i < secondMenuObjects.Length; i++) secondMenuObjects[i].SetActive(true);
    }
    public void DeactiveSecondMenue()
    {
        for (int i = 0; i < secondMenuObjects.Length; i++) secondMenuObjects[i].SetActive(false);
    }
}
