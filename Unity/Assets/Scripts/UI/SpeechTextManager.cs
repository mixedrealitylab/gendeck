using UnityEngine.Events;
using UnityEngine;
using TMPro;
using System;

public class SpeechTextManager : MonoBehaviour
{
    #region events

    // invoced if text is changed
    public UnityEvent<bool> textChanged = new UnityEvent<bool>();

    // invoce this event to change text
    public SingleUnityEvent<string> updateText = new SingleUnityEvent<string>();

    // invoce this event to remove text
    // (does not trigger textChanged)
    public SingleUnityEvent<string> resetText = new SingleUnityEvent<string>();

    #endregion

    #region variables

    public TextMeshProUGUI displayTextObj;
    private static TextMeshProUGUI _displayTextObj;
    private string debugTextPuffer;

    #endregion

    void OnValidate()
    {
        if (displayTextObj == null)
            throw new System.Exception($"{this.GetType().Name}: Display-Text is not defined");
    }

    void Awake()
    {
        SpeechTextManager._displayTextObj = this.displayTextObj;
    }

    #region functions

    public static void UpdateDisplayedText(string text)
    {
        // set text
        SpeechTextManager._displayTextObj.text = text;
        SpeechTextManager._displayTextObj.ForceMeshUpdate(true);
        SpeechTextManager._displayTextObj.enabled = true;
    }

    public static string CurrentText()
    {
        return SpeechTextManager._displayTextObj.text;
    }

    #endregion
}
