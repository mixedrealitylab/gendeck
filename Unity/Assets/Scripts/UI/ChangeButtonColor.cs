using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeButtonColor : MonoBehaviour
{

    //[SerializeField] private GameObject buttonPannel;
    //[SerializeField] private RoundedBoxProperties buttonProperties;
    //public Color selectedColor;
    //public Color originalColor;
    //public Material selectedMaterial;
    //public Material originalMaterial;
    [SerializeField] private GameObject buttonPannel;

    public void Start()
    {
        buttonPannel.SetActive(false);
    }
    // Start is called before the first frame update
    public void ButtonIsSelected()
    {
        /*  Debug.Log("buttonProperties.Color: " + buttonProperties.Color);
          //buttonProperties.Color = new Color(149,243,152,70);
          buttonProperties.Color = selectedColor;
          buttonPannel.GetComponent<Renderer>().material= selectedMaterial;
          buttonPannel.GetComponent<Renderer>().material.color = selectedColor;

          Debug.Log("buttonProperties.Color = new Color(149,243,152,70);");*/
        buttonPannel.SetActive(true);
    }
    public void ChangeColorToTheOrginalColor()
    {
        buttonPannel.SetActive(false);
        /*buttonProperties.Color = originalColor;
        buttonPannel.GetComponent<Renderer>().material = originalMaterial;
        buttonPannel.GetComponent<Renderer>().material.color = originalColor;*/
    }
}
