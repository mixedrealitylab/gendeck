using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeButtonPosition : MonoBehaviour
{
    public GameObject pressedButton;
    public GameObject releasedButton;
    // Start is called before the first frame update
    void Start()
    {
        pressedButton.SetActive(false);
        releasedButton.SetActive(true);
    }

    public void ButtonIsPressed()
    {
        pressedButton.SetActive(true);
        releasedButton.SetActive(false);
    }
    public void ButtonIsReleased()
    {
        pressedButton.SetActive(false);
        releasedButton.SetActive(true);
    }
}
