using UnityEngine.Events;
using UnityEngine;
using TMPro;



public class DebugTextManager : MonoBehaviour
{
    #region variables

    public TextMeshProUGUI displayTextObj;
    private static TextMeshProUGUI _displayTextObj;

    #endregion

    void OnValidate()
    {
        if (displayTextObj == null)
            throw new System.Exception($"{this.GetType().Name}: Display-Text is not defined");
    }

    void Awake()
    {
        DebugTextManager._displayTextObj = this.displayTextObj;
    }

    #region functions

    public static void UpdateDisplayedText(string text)
    {
        // set text
        string currentText = DebugTextManager._displayTextObj.text;
        DebugTextManager._displayTextObj.text = $"{currentText}{text}\n";
        DebugTextManager._displayTextObj.ForceMeshUpdate(true);
        DebugTextManager._displayTextObj.enabled = true;
    }

    public static void ResetDisplayedText(string text)
    {
        // set text
        DebugTextManager._displayTextObj.text = $"{text}\n";
        DebugTextManager._displayTextObj.ForceMeshUpdate(true);
        DebugTextManager._displayTextObj.enabled = true;
    }

    #endregion
}
