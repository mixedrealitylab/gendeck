using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GetDefaultMicrophone : MonoBehaviour
{
    void Start()
    {
        string defaultMicName = GetDefaultMicrophoneName();

        if (!string.IsNullOrEmpty(defaultMicName))
        {
            Debug.Log("Default Microphone: " + defaultMicName);
        }
        else
        {
            throw new System.Exception("No default microphone found.");
        }
    }

    string GetDefaultMicrophoneName()
    {
        string[] microphoneDevices = Microphone.devices;

        if (microphoneDevices.Length > 0)
        {
            return microphoneDevices[0];    
        }

        return null;
    }
}
