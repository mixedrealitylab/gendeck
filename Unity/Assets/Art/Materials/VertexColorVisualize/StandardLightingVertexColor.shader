﻿Shader "Custom/StandardLightingVertexColor"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,0)
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _Glossiness("Smoothness", Range(0,1)) = 0.2 // Reduziere den Wert für Glanz
        _Metallic("Metallic", Range(0,1)) = 0.2 // Reduziere den Wert für metallischen Glanz

        _Contrast("Contrast", Range(0.0, 2.0)) = 1.0

        _RedIntensity("Red Intensity", Range(0.0, 2.0)) = 1.0
        _GreenIntensity("Green Intensity", Range(0.0, 2.0)) = 1.0
        _BlueIntensity("Blue Intensity", Range(0.0, 2.0)) = 1.0
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float4 vColor : COLOR;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        half _Contrast;

        half _Gradient;

        half _RedIntensity;
        half _GreenIntensity;
        half _BlueIntensity;

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * IN.vColor;
    
            c.rgb = (_Contrast * (c.rgb - 0.5)) + 0.5;
    
            c.r *= _RedIntensity;
            c.g *= _GreenIntensity;
            c.b *= _BlueIntensity;
    
            o.Albedo = c.rgb * IN.vColor.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
