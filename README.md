# Speech-To-3D

Convert text/speech into a 3D object and place it into a unity environment.

Example for speech to 3D: [IEEVR-Video](https://youtu.be/4HA5nEaJuF0)

## Description
Proof-of-concept system GenDeck - an application to experience text-to-3D model generation inside an
immersive Virtual Reality environment

# Deployment

## Requirements

- python version >= 3.10.6
- SSH
- Access to Alex at FAU HPC
- Huggingface Account
- (Unity Editor Version 2022.3.3f1)
- (Microsoft Azure Account)
- (Microsoft Azure Speech Service)


### PythonAPI Setup
1. clone repository (use VS Code)
```sh
git clone https://gitlab.com/mixedrealitylab/gendeck.git

# initialize LFS and pull files
git lfs install
git lfs pull

```

2. change opened folder in VS Code to PythonAPI
3. use python to start startup.py
```sh
python startup.py
```

answer following setup steps:
- step 1: create rsa-key -> you can find the key in PythonAPI/config_data
(you need to register this ssh key in the hpc portal: https://portal.hpc.fau.de/)

- step 2: define config data

following questions:

(if you want the default-value press enter-key)

- application secret -> with this secret you can use the default user(name = user, password = password)



you can add new user if the API is running with '<url>/auth/' and the request body schema
```sh
{
    "username": "string",
    "password": "string"
}
```

- algorithm -> the default algorithm is 'HS256'
- time token -> the default time until the token expire is '300'
- API job limit -> the default is '3'
- Huggingface Token -> use your huggingface access token (https://huggingface.co/settings/tokens)
- huggingface max request fails -> the default is '10'
- alex vault path -> the default is ''.
    You only need it if you do not want the main folder in your home directory of your vault
 - modelFolder -> the default name is '0-model-calculation'. It is the name of your main folder.
!!! You need to create this folder in your vault home directory, needed for HPC-setup !!!
- group name: use your HPC group name (the default 'b116ba')
- user name: user your HPC user name

- step 3: create ssh config
    in this step you do not need to do anything

- step 4: create python environment
    the script creates an python environement .venv and install all needed packages


### HPC Setup
In this step you need to connect to your HPC alex vault directory

1. create in the main folder for all models (modelFolder)
```sh
mkdir 0-model-calculation
```
2. Creating singularity images for the following models and relocating them to the HPC/models directory:
   - [Wonder3D](https://github.com/xxlong0/Wonder3D)
   - [One-2-3-45](https://github.com/One-2-3-45/One-2-3-45)

3. upload the content of the folder speech-to-3d > HPC > model into the main folder.

(I could take a while ☕)

4. run hpc setup script
```sh
python3 hpc_startup.py
```

after this you have two folders (...-wonder3d and ...-one2345) in your main folder 0-model-calculation
Delete the copied folders and setup file

## Start API
Return to PythonAPI folder in your IDE

```sh
# your location is PythonAPI/

# activate python environment
# use: windows: '.\\.venv\\Scripts\\activate' or linux: 'activate .venv/bin/activate'

# change directory to src
cd src

# start API
uvicorn main:app --host 0.0.0.0 --port 8000

# check swagger API
# open in your browser: http://localhost:8000/docs
```

use the Jupyter Notebook script 'run.ipynb' in speech-to-3d > PythonAPI to test the API


## Unity Setup


Full Unity instructions: [Instructions](https://youtu.be/x14hzO-E2Ek)


### Short instructions
1. Open Unity folder with Unity Hub - use the correct version (2022.3.3f1)

2. On the left side click on "--- SST ---"

3. Change the API Key and the region based on your Azure Service configuration

4. On the left side click on "--- API ---"

5. Change the API URL based on your API path

6. Select the correct model

7. Define username and password of your API user


## Citation

If you find this project useful for your research or use any of the code or data provided here, please consider citing our paper:
```latex
@inproceedings{weid2024gendeck,
  title={GenDeck: Towards a HoloDeck with Text-to-3D Model Generation},
  author={Weid, Manuel and Khezrian, Navid and Mana, Aparna Pindali and Farzinnejad, Forouzan and Grubert, Jens},
  booktitle={Proceedings of IEEE Conference Virtual Reality and 3D User Interfaces},
  pages={1184--1185},
  year={2024}
}
```