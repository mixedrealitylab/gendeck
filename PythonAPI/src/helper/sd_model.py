from enum import Enum, EnumMeta



class MetaEnum(EnumMeta):
    # validation if string value is a value in enum
    def __contains__(cls, item):
        try:
            cls(item)
        except ValueError:
            return False
        return True


class BaseEnum(Enum, metaclass=MetaEnum):
    @classmethod
    def from_str(cls, convertString):
        if convertString in cls:
            for member in cls:
                if member.value == convertString:
                    return member
        else:
            return None


class StableDiffusionModel(BaseEnum):
    SD_1_5 = "sd_1_5"
    SX_XL = "sd_XL"
