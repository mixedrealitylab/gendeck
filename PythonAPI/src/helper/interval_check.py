from fastapi import HTTPException
import asyncio


async def periodic_status_check(parameter, func, check_time, max_check_time):
    counter = 0
    
    while True:
        if counter > max_check_time:
            raise HTTPException(status_code=status.HTTP_410_GONE, detail="Status check failed")
        
        status = func(parameter)
            
        if not status:
            counter += check_time
            await asyncio.sleep(check_time)
        else:
            return