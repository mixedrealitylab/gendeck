import re
from model.schema.data import DefaultModel
from helper.sd_model import StableDiffusionModel
from fastapi import Form, HTTPException, status
from io import BytesIO
from PIL import Image


def validate_prompt(prompt: str):
    if len(prompt) < 2:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Invalid text input by length",
        )

    # validate prompt content
    text_regex = re.compile(r'^[a-zA-Z0-9][a-zA-Z0-9\s\.,!?]*$')

    if not text_regex.match(prompt):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Invalid text input by character combination",
        )


def validate_pid(pid: str):
    if len(pid) < 2:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Do not found a valid pid",
        )

def validate_pid_exists(pid: str, checkFunc):
    if checkFunc(pid=pid) == False:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Unknown pid ",
        )

def validate_image_count(imageCount : int):
    if(imageCount <= 0):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Image count is to low",
        )


def validate_list_number(number : int, listSize: int):
    if(number <= 0):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Number can not be 0 or less",
        )

    if(number > listSize):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Number can not be greater than count of used list",
        )

def validate_StableDiffusion_model(modelName: str):
    if(modelName not in StableDiffusionModel):
         raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Unknown StableDiffusion-modelname",
        )


def validate_ModelData_has_variable(modelData: DefaultModel, variableName: str):

    has_variable = hasattr(modelData, variableName)

    if(not has_variable):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"ProcessModel has no {variableName.replace('_', ' ')} information",
        )


def validate_image_content(image: bytes):
    try:
        # Attempt to open the image
        img = Image.open(BytesIO(image))

        # Check if the image format is supported
        if(img.format is None):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Checked image has a unsupported image format",
            )

    except Exception as e:
        # Attempt failed
        raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Can not use the passed image",
            )

def validation_job_id(jobId: str):

    # check if jobId is none
    if(jobId == None or jobId == ""):
        raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Undefined job id",
            )

def validate_image_list(imageList: list):

    # check if list exists
    if not imageList:
        raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"No images found",
            )