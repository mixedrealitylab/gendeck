from enum import Enum


class Status(Enum):
    IDLE = 0
    RUNNING = 1
    WAIT_FOR_CLEANING = 2
    FINISHED = 3
    ERROR = 99
    