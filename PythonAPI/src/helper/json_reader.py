import json


def read_json_file(filePath: str):
    with open(filePath, "r") as file:
        data = json.load(file)
    return data