from typing import Annotated, Optional

from fastapi import APIRouter, Depends, Form, status
from sqlalchemy.orm import Session
from helper.sd_model import StableDiffusionModel

from model.process.convertTypes import convertBinaryList_to_base64List
from model.process.ProcessInfo import ProcessInfo
from model.process.TextToModelFunctions import TextToModelFunctions
from model.schema.textToModel_dataContainer import DataContainerAPI
from security.auth import get_current_user, get_db

# ------------------------------ General ------------------------------
CURRENT_MODEL = "0000-model-wonder3d"   # HPC foldername
ROUTE_NAME = 'wonder3d'                 # API general route


# ------------------------- General Route -----------------------------
router = APIRouter(
    prefix=f'/model/{ROUTE_NAME}',
    tags=[ROUTE_NAME]
)

# ---------------------------- Dependencies-----------------------------
db_dependency = Annotated[Session, Depends(get_db)]
user_dependency = Annotated[dict, Depends(get_current_user)]

# ------------------------------ Routes --------------------------------

# ------------------------------------------------------------------------
# ----------------------------- upload prompt ----------------------------
# ------------------------------------------------------------------------
@router.post("/upload_prompt", status_code=status.HTTP_200_OK)
async def upload_prompt(user: user_dependency, db: db_dependency, prompt: str = Form(...)):

    # register new process to system
    pid = TextToModelFunctions.createNewProcess(
        modelDataSchema=DataContainerAPI,
        prompt=prompt,
        usedModel=CURRENT_MODEL
    )

    return {'pid': pid}



# ------------------------------------------------------------------------
# ---------------------------- generate image ----------------------------
# ------------------------------------------------------------------------
@router.post("/generate_image", status_code=status.HTTP_200_OK)
async def generate_image(user: user_dependency, db: db_dependency,
                        pid: str = Form(...),
                        useHpc: bool = Form(...),
                        imageCount: int = Form(...),
                        sdModelName: str  = Form(...),
                        useImageNr: int = Form(...),
                        ):

    # ------- Define default values -------
    if(useHpc == None):
        useHpc = False

    if(imageCount == None):
        imageCount = 1

    if(sdModelName == None):
        sdModelName = StableDiffusionModel.SD_1_5.name

    if(useImageNr == None):
        useImageNr = 1

    # ------- Switch between HPC and Hugging Face -------

    if(useHpc):
        # generate image with HPC
        await TextToModelFunctions.hpcGeneratedImages(
            pid=pid,
            stableDiffusionModelName=sdModelName,
            imageCount=imageCount
        )
    else:
        # generate image with Hugging Face
        await TextToModelFunctions.huggingFaceGeneratedImages(
            pid=pid,
            imageCount=imageCount
        )

    # handle generated images
    TextToModelFunctions.handleGeneratedImages(
            pid=pid,
            useImageNr=useImageNr
        )



# ------------------------------------------------------------------------
# ---------------------------- select image ----------------------------
# ------------------------------------------------------------------------
@router.post("/get_generated_image", status_code=status.HTTP_200_OK)
async def generated_image(user: user_dependency, db: db_dependency, pid: str = Form(...), getImageNr : int = Form(...)):
    """ Return generated images

    Args:
        pid (str, optional): process id
        getImageNr (int, optional): image number, starts with 1 (by using -1 it returns all images)

    Returns:
        _type_: _description_
    """


    binaryImageList : list = []

    # get generated images
    binaryImageList = TextToModelFunctions.getGeneratedImage(
            pid=pid,
            getImageNr=getImageNr
        )

    base64ImageList = convertBinaryList_to_base64List(binaryImageList)

    return {
            'imagesBase64': base64ImageList
        }



# ------------------------------------------------------------------------
# ---------------------------- select image ----------------------------
# ------------------------------------------------------------------------
@router.post("/select_image", status_code=status.HTTP_200_OK)
async def select_image(user: user_dependency, db: db_dependency, pid: str = Form(...), useImageNr : int = Form(...)):

    # change selected image
    TextToModelFunctions.handleGeneratedImages(
            pid=pid,
            useImageNr=useImageNr
        )



# ------------------------------------------------------------------------
# --------------------------- generate 3D model --------------------------
# ------------------------------------------------------------------------
@router.post("/generate_model", status_code=status.HTTP_200_OK)
async def generate_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # start 3d job
    TextToModelFunctions.generated3dModelWithImage(
            pid=pid
        )



# ------------------------------------------------------------------------
# ----------------------------- status of run ----------------------------
# ------------------------------------------------------------------------
@router.post("/status_model", status_code=status.HTTP_200_OK)
async def status_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # check status of 3d job
    TextToModelFunctions.updateStatus3dModel(
            pid=pid
        )

    start_time = ProcessInfo.processData[pid].startTime
    job_status = ProcessInfo.processData[pid].model_status.name

    return {
            'status': job_status,
            'startTime': start_time
        }



# ------------------------------------------------------------------------
# --------------------------- download 3D model --------------------------
# ------------------------------------------------------------------------
@router.post("/download_model", status_code=status.HTTP_200_OK)
async def download_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # check for of 3d model content
    content = TextToModelFunctions.get3dModel(
            pid=pid,
            modelFileName="mesh.obj"
        )

    current_status = ProcessInfo.processData[pid].model_status

    return {
            'status': current_status,
            'content': content
        }



# ------------------------------------------------------------------------
# ------------------------------- clean up ------------------------------
# ------------------------------------------------------------------------
@router.post("/clean_up", status_code=status.HTTP_200_OK)
async def clean_up(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    """
    Remove Input and Output files based on pid from HPC
    """

    TextToModelFunctions.removeInputOutputFromHPC(
            pid=pid
        )


# ------------------------------------------------------------------------
# ------------------------------ delete run ------------------------------
# ------------------------------------------------------------------------
@router.post("/delete_run", status_code=status.HTTP_200_OK)
async def delete_run(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    """
    Remove process id from system
    """

    TextToModelFunctions.removeProcessFromSystem(
            pid=pid
        )



# ------------------------------------------------------------------------
# --------------- remove hpc content and api process ---------------------
# ------------------------------------------------------------------------
@router.post("/cleanAndDelete", status_code=status.HTTP_200_OK)
async def clean_delete_run(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    TextToModelFunctions.removeInputOutputFromHPC(
            pid=pid
        )

    TextToModelFunctions.removeProcessFromSystem(
            pid=pid
        )
