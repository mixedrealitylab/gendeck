import base64


def convertBinaryList_to_base64List(binary_list):

    base64_image_list = []

    # iterate binary list
    for binary_img in binary_list:

        # encode decode on base64
        base64_img = base64.b64encode(binary_img).decode('utf-8')
        base64_image_list.append(base64_img)

    return base64_image_list
