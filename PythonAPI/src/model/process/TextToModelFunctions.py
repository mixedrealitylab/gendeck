import datetime
from helper.status import Status
from processing.ModelProcessing import ModelProcessing
from processing.TextToImage import text_to_image
from processing.localTextToImage import text_to_images
from helper.decorator.validation import validation_job_id, validate_image_content, validate_prompt, validate_pid, validate_pid_exists, validate_image_count, validate_StableDiffusion_model, validate_ModelData_has_variable, validate_list_number
from helper.sd_model import StableDiffusionModel
from model.process.ProcessInfo import ProcessInfo
from model.schema.data import DefaultModel
from model.schema.demo_data import Model
from time import time, sleep
import logging


class TextToModelFunctions():

    @staticmethod
    def createNewProcess(modelDataSchema, prompt: str, usedModel: str):
        """
        Register new process to system

        Returns:
            str: pid
        """

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start register new process: model = {usedModel} prompt = '{prompt}'")

        # validate prompt
        validate_prompt(prompt=prompt)

        # register new runtime process model
        pid = ProcessInfo.registerNewModel(modelType=modelDataSchema)

        # add info
        ProcessInfo.processData[pid].startTime = time()
        ProcessInfo.processData[pid].prompt_content = prompt
        ProcessInfo.processData[pid].modelName = usedModel

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished register new process: model = {usedModel} prompt = '{prompt}'")

        return pid

    @staticmethod
    async def hpcGeneratedImages(pid: str, stableDiffusionModelName: str, imageCount: int = 1):
        """
        Generate and save image with StableDiffusion on HPC

        Args:
            pid (str): current process data id
            model (str): StableDiffusion model name
            imageCount (int, optional): generated image count. Defaults to 1.
        """

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start generate image - HPC: stableDiffusionModelName = {stableDiffusionModelName} imageCount = {imageCount}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        modelData = ProcessInfo.processData[pid]

        # validate ProcessData has variable image_content_selection
        validate_ModelData_has_variable(modelData=modelData, variableName="image_content_selection")

        # validate image count
        validate_image_count(imageCount=imageCount)

        # validate StableDiffusion model name
        validate_StableDiffusion_model(modelName=stableDiffusionModelName)

        # convert StableDiffusion model name
        stableDiffusionModel = StableDiffusionModel.from_str(stableDiffusionModelName)

        # validate saved prompt
        prompt = ProcessInfo.processData[pid].prompt_content
        validate_prompt(prompt=prompt)

        binary_images = []

        # generate images
        binary_images: list = await text_to_images(
            prompt=prompt,
            count=imageCount,
            stableDiffusionModel=stableDiffusionModel
        )

        # save generated image in api process
        ProcessInfo.processData[pid].image_content_selection = binary_images

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished generate image - HPC: stableDiffusionModelName = {stableDiffusionModelName} imageCount = {imageCount} prompt = {prompt}")

    @staticmethod
    async def huggingFaceGeneratedImages(pid: str, imageCount: int = 1):

        """
        Generate and save image with Hugging Face
        """

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start generate image - HuggingFace: imageCount = {imageCount}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        modelData = ProcessInfo.processData[pid]

        # validate ProcessData has variable image_content_selection
        validate_ModelData_has_variable(modelData=modelData, variableName="image_content_selection")

        # validate image count
        validate_image_count(imageCount=imageCount)

        # validate saved prompt
        prompt = ProcessInfo.processData[pid].prompt_content
        validate_prompt(prompt=prompt)

        # generate images
        binary_images: list = text_to_image(prompt=prompt, count=imageCount)

        ProcessInfo.processData[pid].image_content_selection = binary_images

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished generate image - HuggingFace: imageCount = {imageCount} prompt = {prompt}")


    @staticmethod
    def selectProcessImage(pid: str, image_nr: int):

        """
        Select image for model generation based on image number
        """

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start select image: image_nr = {image_nr}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        modelData = ProcessInfo.processData[pid]

        # validate ProcessData has variable image_content_selection
        validate_ModelData_has_variable(modelData=modelData, variableName="image_content_selection")

        imageList : list = getattr(modelData, "image_content_selection")
        imageListSize : int = len(imageList)

        # validate image nr
        validate_list_number(number=image_nr, listSize=imageListSize)

        # select image
        ProcessInfo.processData[pid].image_content = imageList[image_nr-1]

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished select image: image_nr = {image_nr}")


    @staticmethod
    def handleGeneratedImages(pid: str, useImageNr: int):

        """
        Preselecting and uploading images to HPC
        """

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start handle generated image: useImageNr = {useImageNr}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        modelData = ProcessInfo.processData[pid]

        # validate ProcessData has variable image_content_selection
        validate_ModelData_has_variable(modelData=modelData, variableName="image_content_selection")

        imageList : list = getattr(modelData, "image_content_selection")
        imageListSize : int = len(imageList)

        # validate image nr
        validate_list_number(number=useImageNr, listSize=imageListSize)

        # if only one image exists
        if(imageListSize == 1):

            # select image
            TextToModelFunctions.selectProcessImage(pid=pid, image_nr=1)

            # upload image
            model = modelData.modelName
            binary_image = imageList[0]
            upload_status = ModelProcessing.copy_image_content(image_content=binary_image, model_name=model, pid=pid)

        # predefined user selection
        else:

            # select image
            TextToModelFunctions.selectProcessImage(pid=pid, image_nr=useImageNr)

            # upload image
            model = modelData.modelName
            binary_image = ProcessInfo.processData[pid].image_content
            upload_status = ModelProcessing.copy_image_content(image_content=binary_image, model_name=model, pid=pid)

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished handle generated image: useImageNr = {useImageNr}")


    @staticmethod
    def getGeneratedImage(pid: str, getImageNr: int):

        """
        Get binary image of model generation based on image number

        Returns:
            list[byte]: binary image data
        """

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start get generated image: getImageNr = {getImageNr}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        modelData = ProcessInfo.processData[pid]

        # validate ProcessData has variable image_content_selection
        validate_ModelData_has_variable(modelData=modelData, variableName="image_content_selection")

        imageList : list = getattr(modelData, "image_content_selection")
        imageListSize : int = len(imageList)

        # return all images
        if(getImageNr == -1):

            # debug message
            logging.info(f"[{TextToModelFunctions.__name__}] Finished get generated image: getImageNr = {getImageNr}, return image list")

            return imageList

        # validate image nr
        validate_list_number(number=getImageNr, listSize=imageListSize)

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished get generated image: getImageNr = {getImageNr}")

        # return selected image
        return [imageList[getImageNr-1]]


    @staticmethod
    def generated3dModelWithImage(pid: str):

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start generated 3d model: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        # validate image
        binary_image = ProcessInfo.processData[pid].image_content
        validate_image_content(image=binary_image)

        modelName = ProcessInfo.processData[pid].modelName

        # start hpc job
        job_id = ModelProcessing.start_job(pid=pid, model_name=modelName, input_file_extension="png")

        # define job start parameters
        ProcessInfo.processData[pid].job_id = job_id
        ProcessInfo.processData[pid].model_status = Status.RUNNING

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished generated 3d model: pid = {pid}")


    @staticmethod
    def generated3dModelWithOnlyPrompt(pid: str):

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start generated 3d model: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        # validate prompt
        prompt = ProcessInfo.processData[pid].prompt_content
        validate_prompt(prompt=prompt)

        modelName = ProcessInfo.processData[pid].modelName

        # start hpc job
        job_id = ModelProcessing.start_job(pid=pid, model_name=modelName, input_file_extension="", parameters=[pid, f"'{prompt}'"])

        # define job start parameters
        ProcessInfo.processData[pid].job_id = job_id
        ProcessInfo.processData[pid].model_status = Status.RUNNING

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished generated 3d model: pid = {pid}")



    @staticmethod
    def get3dModel(pid: str, modelFileName: str):

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start get 3d model: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        # update job status
        TextToModelFunctions.updateStatus3dModel(pid)
        current_status = ProcessInfo.processData[pid].model_status

        # job is running
        if(current_status == Status.RUNNING): return ""

        # model name of running job
        model = ProcessInfo.processData[pid].modelName

        # convert obj
        CONVERT_EXT = ".dae"
        MAX_TIME = 2 * 60 # 2 minutes
        result = False
        newFilePath = ModelProcessing.convert_obj_file(pid=pid, model_name=model, obj_name=modelFileName, convert_extension=CONVERT_EXT)
        startCheckTime = datetime.datetime.now()

        while(result == False):
            sleep(4)

            now = datetime.datetime.now()

            # check if converted file exists
            result = ModelProcessing.check_file_exist(newFilePath)

            if(result):
                modelFileName = modelFileName.replace(".obj", CONVERT_EXT)
                logging.info("change .obj to .dae")
                break
            else:
                difference = now - startCheckTime
                differenceSeconds = difference.total_seconds()

                if differenceSeconds >= MAX_TIME:
                    logging.error("Problem to convert .obj to .dae")
                    break

                continue

        # download model
        object_content = ModelProcessing.download_object(pid=pid, model_name=model, obj_name=modelFileName)

        # sign model
        # object_content = ModelProcessing.add_comment_to_obj_content(content=object_content, model_name=model)

        # save current model result
        ProcessInfo.processData[pid].object_content = object_content

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished get 3d model: pid = {pid}")

        return object_content


    @staticmethod
    def updateStatus3dModel(pid: str):

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start update 3d model generation status: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        job_id = ProcessInfo.processData[pid].job_id

        # validate job id
        validation_job_id(jobId=job_id)

        start_time = ProcessInfo.processData[pid].startTime

        # get current status of job as boolean
        status_job = ModelProcessing.check_job(job_id=job_id)

        if(status_job):
            ProcessInfo.processData[pid].model_status = Status.RUNNING
        else:
            ProcessInfo.processData[pid].model_status = Status.WAIT_FOR_CLEANING

            endTime = time()
            ProcessInfo.processData[pid].endTime = endTime

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished update 3d model generation status: pid = {pid} status = {ProcessInfo.processData[pid].model_status}")


    @staticmethod
    def removeInputOutputFromHPC(pid: str):

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start remove input and output of model from HPC: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        job_id = ProcessInfo.processData[pid].job_id
        validation_job_id(jobId=job_id)

        model = ProcessInfo.processData[pid].modelName

        # remove image
        delete_image_status = ModelProcessing.remove_image_content(pid=pid, model_name=model)

        # remove obj file
        delete_obj_status = ModelProcessing.remove_object_content(pid=pid, model_name=model)

        # change status
        ProcessInfo.processData[pid].model_status = Status.FINISHED

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished remove input and output of model from HPC: pid = {pid}")


    @staticmethod
    def removeProcessFromSystem(pid: str):

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Start remove process from API: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        # remove process
        ProcessInfo.removeModel(pid=pid)

        # debug message
        logging.info(f"[{TextToModelFunctions.__name__}] Finished remove process from API: pid = {pid}")



