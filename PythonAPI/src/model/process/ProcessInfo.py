from model.schema.data import DefaultModel
from helper.status import Status
import uuid


class ProcessInfo():

    # -----------------------------------
    # ---------- data storage -----------
    # -----------------------------------
    processData = dict()

    # create new process
    @staticmethod
    def registerNewModel(modelType):

        pid = uuid.uuid4().hex
        ProcessInfo.processData[pid] = modelType()
        ProcessInfo.processData[pid].id = pid
        ProcessInfo.processData[pid].status = Status.IDLE
        return pid

    # remove process
    @staticmethod
    def removeModel(pid: str):
        ProcessInfo.processData.pop(pid)

    # check if process exists
    @staticmethod
    def pidInData(pid: str):
        if pid in ProcessInfo.processData:
            return True
        return False
