import base64
import logging
from time import time

from helper.decorator.validation import validate_ModelData_has_variable, validate_image_count, validate_image_list, validate_pid, validate_pid_exists, validate_prompt, validation_job_id
from helper.status import Status
from model.process.ProcessInfo import ProcessInfo
from processing.ModelProcessing import ModelProcessing


class StableDiffusionFunctions():

    @staticmethod
    def createNewProcess(modelDataSchema, prompt: str, usedModel: str):
        """
        Register new process to system

        Returns:
            str: pid
        """

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Start register new process: model = {usedModel} prompt = '{prompt}'")

        # validate prompt
        validate_prompt(prompt=prompt)

        # register new runtime process model
        pid = ProcessInfo.registerNewModel(modelType=modelDataSchema)

        # add info
        ProcessInfo.processData[pid].startTime = time()
        ProcessInfo.processData[pid].prompt_content = prompt
        ProcessInfo.processData[pid].modelName = usedModel

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Finished register new process: model = {usedModel} prompt = '{prompt}'")

        return pid

    @staticmethod
    def generateImages(pid: str, image_count : int):

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Start generate images: pid = {pid} image_count = {image_count}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        # validate dataContainer object
        modelData = ProcessInfo.processData[pid]
        validate_ModelData_has_variable(modelData=modelData, variableName="prompt_content")

        # validate prompt
        prompt = ProcessInfo.processData[pid].prompt_content
        validate_prompt(prompt=prompt)

        model = ProcessInfo.processData[pid].modelName

        validate_image_count(imageCount=image_count)
        ProcessInfo.processData[pid].image_count = image_count

        # start StableDiffusion hpc job
        job_id = ModelProcessing.start_job(pid=pid, model_name=model, input_file_extension="-", parameters=[f'"{prompt}"', image_count])

        # define job start parameters
        ProcessInfo.processData[pid].job_id = job_id
        ProcessInfo.processData[pid].model_status = Status.RUNNING

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Finish generate images: pid = {pid} image_count = {image_count}")

    @staticmethod
    def getImages(pid: str):

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Start download images: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        # validate dataContainer object
        modelData = ProcessInfo.processData[pid]
        validate_ModelData_has_variable(modelData=modelData, variableName="image_content")

        # update job status
        StableDiffusionFunctions.updateStatusStableDiffusionModel(pid)
        current_status = ProcessInfo.processData[pid].model_status

        binaryImageList = []

        # job is running
        if(current_status == Status.RUNNING): return binaryImageList

        # model name of running job
        model = ProcessInfo.processData[pid].modelName

        image_count = ProcessInfo.processData[pid].image_count
        validate_image_count(imageCount=image_count)

        for image_nr in range(0, image_count):

            # first image start with 1
            img_name = f"img_{image_nr+1}.png"

            # download the image from hpc
            binaryImage = ModelProcessing.download_image(pid=pid, model_name=model, image_name=img_name)

            # add image to result list
            binaryImageList.append(binaryImage)

        ProcessInfo.processData[pid].image_content = binaryImageList

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Finish download images: pid = {pid}")
        logging.info(f"[{StableDiffusionFunctions.__name__}] image_count = {image_count} => return binary list = {len(binaryImageList)}")

        return binaryImageList


    @staticmethod
    def updateStatusStableDiffusionModel(pid: str):

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Start update StableDiffusion model generation status: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        job_id = ProcessInfo.processData[pid].job_id

        # validate job id
        validation_job_id(jobId=job_id)

        # get current status of job as boolean
        status_job = ModelProcessing.check_job(job_id=job_id)

        if(status_job):
            ProcessInfo.processData[pid].model_status = Status.RUNNING
        else:
            ProcessInfo.processData[pid].model_status = Status.WAIT_FOR_CLEANING

            endTime = time()
            ProcessInfo.processData[pid].endTime = endTime

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Finished update StableDiffusion model generation status: pid = {pid} status = {ProcessInfo.processData[pid].model_status}")

    @staticmethod
    def removeInputOutputFromHPC(pid: str):

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Start remove input and output of model from HPC: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        job_id = ProcessInfo.processData[pid].job_id
        validation_job_id(jobId=job_id)

        model = ProcessInfo.processData[pid].modelName

        # remove image
        delete_image_status = ModelProcessing.remove_image_content(pid=pid, model_name=model)

        # remove obj file
        delete_obj_status = ModelProcessing.remove_object_content(pid=pid, model_name=model)

        # change status
        ProcessInfo.processData[pid].model_status = Status.FINISHED

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Finished remove input and output of model from HPC: pid = {pid}")

    @staticmethod
    def removeProcessFromSystem(pid: str):

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Start remove process from API: pid = {pid}")

        # validate pid
        validate_pid(pid=pid)
        validate_pid_exists(pid=pid, checkFunc=ProcessInfo.pidInData)

        # remove process
        ProcessInfo.removeModel(pid=pid)

        # debug message
        logging.info(f"[{StableDiffusionFunctions.__name__}] Finished remove process from API: pid = {pid}")

    @staticmethod
    def convertBinaryListToBase64(binaryImageList):
        # check if images are present
        validate_image_list(binaryImageList)

        return [base64.b64encode(image).decode("utf-8") for image in binaryImageList]
