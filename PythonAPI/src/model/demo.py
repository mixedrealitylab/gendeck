from helper.decorator.validation import validate_prompt, validate_pid, validate_pid_exists
from fastapi import Form, status, Depends, APIRouter, BackgroundTasks
#from apscheduler.schedulers.background import BackgroundScheduler
from helper.interval_check import periodic_status_check
from processing.ModelProcessing import ModelProcessing
from security.auth import get_current_user, get_db
from processing.TextToImage import text_to_image
from model.schema.demo_data import Model
from model.process.ProcessInfo import ProcessInfo
from sqlalchemy.orm import Session
from helper.status import Status
from typing import Annotated
import trimesh
import logging
import time
import os


# ------------------------------ general ------------------------------
CURRENT_MODEL = "0000-model-demo"
DELETE_TIME_SECONDS = 300
SCHEDULER_RUN_TIME_MINUTES = 15
DEMO_FILE = r'./data/demo/demoMesh.obj'

router = APIRouter(
    prefix='/model/demo',
    tags=['demo']
)

db_dependency = Annotated[Session, Depends(get_db)]
user_dependency = Annotated[dict, Depends(get_current_user)]


@router.post("/clean", status_code=status.HTTP_200_OK)
def cleaner():
    return status.HTTP_200_OK


# ------------------------------------------------------------------------
# ----------------------------- upload prompt ----------------------------
# ------------------------------------------------------------------------
@router.post("/upload_prompt", status_code=status.HTTP_200_OK)
async def upload_prompt(user: user_dependency, db: db_dependency, prompt: str = Form(...)):
    return {'pid': "123"}



# ------------------------------------------------------------------------
# ---------------------------- generate image ----------------------------
# ------------------------------------------------------------------------
@router.post("/generate_image", status_code=status.HTTP_200_OK)
async def generate_image(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    return status.HTTP_200_OK



# ------------------------------------------------------------------------
# --------------------------- generate 3D model --------------------------
# ------------------------------------------------------------------------
@router.post("/generate_model", status_code=status.HTTP_200_OK)
async def generate_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    return status.HTTP_200_OK



# ------------------------------------------------------------------------
# --------------------------- download 3D model --------------------------
# ------------------------------------------------------------------------
@router.post("/download_model", status_code=status.HTTP_200_OK)
async def download_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    path = os.getcwd()

    if(not os.path.exists(path + "/" + DEMO_FILE)):
            logging.info("can not find demo mesh file")

    with open(DEMO_FILE, 'r') as obj_file:
        object_content = obj_file.read()


    return {
            'status': Status.WAIT_FOR_CLEANING,
            'content': object_content
        }


# ------------------------------------------------------------------------
# ----------------------------- status of run ----------------------------
# ------------------------------------------------------------------------
@router.post("/status", status_code=status.HTTP_200_OK)
async def status_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    return {
            'status': Status.WAIT_FOR_CLEANING,
            'startTime': time.time()
        }



# ------------------------------------------------------------------------
# ------------------------------- clean up ------------------------------
# ------------------------------------------------------------------------
@router.post("/clean_up", status_code=status.HTTP_200_OK)
async def clean_up(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    return status.HTTP_200_OK


# ------------------------------------------------------------------------
# ------------------------------ delete run ------------------------------
# ------------------------------------------------------------------------
@router.post("/delete_run", status_code=status.HTTP_200_OK)
async def delete_run(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    return status.HTTP_200_OK



# ------------------------------------------------------------------------
# ------------------------------ all in one ------------------------------
# ------------------------------------------------------------------------
#user: user_dependency, db: db_dependency, prompt: str = Form(...)
@router.post("/aio", status_code=status.HTTP_200_OK)
async def single_model_request(user: user_dependency, db: db_dependency, prompt: str = Form(...)):
    return trimesh.load(DEMO_FILE)