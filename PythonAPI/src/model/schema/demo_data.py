from model.schema.data import DefaultModel

class Model(DefaultModel):
    prompt_content: str
    image_content: str
    object_content: str