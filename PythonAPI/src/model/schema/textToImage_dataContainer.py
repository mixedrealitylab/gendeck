from model.schema.data import DefaultModel

class DataContainerAPI(DefaultModel):
    prompt_content: str = ""
    image_count: int = 0
    image_content: list = []