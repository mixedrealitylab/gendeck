from model.schema.data import DefaultModel

class DataContainerAPI(DefaultModel):
    prompt_content: str = ""
    image_content: str = ""
    object_content: str = ""
    image_content_selection: list = []