from helper.status import Status

class DefaultModel():
    id: str = ""
    job_id: int = None
    modelName: str = ""
    startTime: float = 0.0
    endTime: float = 0.0
    model_status: Status = Status.IDLE