from helper.decorator.validation import validate_prompt, validate_pid, validate_pid_exists
from fastapi import Form, status, Depends, APIRouter, BackgroundTasks
#from apscheduler.schedulers.background import BackgroundScheduler
from helper.interval_check import periodic_status_check
from model.process.TextToModelFunctions import TextToModelFunctions
from processing.ModelProcessing import ModelProcessing
from security.auth import get_current_user, get_db
from processing.TextToImage import text_to_image
from model.schema.textToModel_dataContainer import DataContainerAPI
from model.process.ProcessInfo import ProcessInfo
from sqlalchemy.orm import Session
from helper.status import Status
from typing import Annotated
import logging
import time
import io


# ------------------------------ general ------------------------------
CURRENT_MODEL = "0000-model-eShapeTextTo3d"   # HPC foldername
ROUTE_NAME = 'eShapeTextTo3d'                 # API general route


# ------------------------- General Route -----------------------------
router = APIRouter(
    prefix=f'/model/{ROUTE_NAME}',
    tags=[ROUTE_NAME]
)

# ---------------------------- Dependencies-----------------------------
db_dependency = Annotated[Session, Depends(get_db)]
user_dependency = Annotated[dict, Depends(get_current_user)]

# ------------------------------ Routes --------------------------------

# ------------------------------------------------------------------------
# ----------------------------- upload prompt ----------------------------
# ------------------------------------------------------------------------
@router.post("/upload_prompt", status_code=status.HTTP_200_OK)
async def upload_prompt(user: user_dependency, db: db_dependency, prompt: str = Form(...)):

    # register new process to system
    pid = TextToModelFunctions.createNewProcess(
        modelDataSchema=DataContainerAPI,
        prompt=prompt,
        usedModel=CURRENT_MODEL
    )

    return {'pid': pid}



# ------------------------------------------------------------------------
# --------------------------- generate 3D model --------------------------
# ------------------------------------------------------------------------
@router.post("/generate_model", status_code=status.HTTP_200_OK)
async def generate_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # start 3d job
    TextToModelFunctions.generated3dModelWithOnlyPrompt(
            pid=pid
        )



# ------------------------------------------------------------------------
# ----------------------------- status of run ----------------------------
# ------------------------------------------------------------------------
@router.post("/status_model", status_code=status.HTTP_200_OK)
async def status_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # check status of 3d job
    TextToModelFunctions.updateStatus3dModel(
            pid=pid
        )

    start_time = ProcessInfo.processData[pid].startTime
    job_status = ProcessInfo.processData[pid].model_status.name

    return {
            'status': job_status,
            'startTime': start_time
        }



# ------------------------------------------------------------------------
# --------------------------- download 3D model --------------------------
# ------------------------------------------------------------------------
@router.post("/download_model", status_code=status.HTTP_200_OK)
async def download_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # check for of 3d model content
    content = TextToModelFunctions.get3dModel(
            pid=pid,
            modelFileName="model.obj"
        )

    current_status = ProcessInfo.processData[pid].model_status

    return {
            'status': current_status,
            'content': content
        }



# ------------------------------------------------------------------------
# ------------------------------- clean up ------------------------------
# ------------------------------------------------------------------------
@router.post("/clean_up", status_code=status.HTTP_200_OK)
async def clean_up(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    """
    Remove Input and Output files based on pid from HPC
    """

    TextToModelFunctions.removeInputOutputFromHPC(
            pid=pid
        )


# ------------------------------------------------------------------------
# ------------------------------ delete run ------------------------------
# ------------------------------------------------------------------------
@router.post("/delete_run", status_code=status.HTTP_200_OK)
async def delete_run(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    """
    Remove process id from system
    """

    TextToModelFunctions.removeProcessFromSystem(
            pid=pid
        )



# ------------------------------------------------------------------------
# --------------- remove hpc content and api process ---------------------
# ------------------------------------------------------------------------
@router.post("/cleanAndDelete", status_code=status.HTTP_200_OK)
async def clean_delete_run(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    TextToModelFunctions.removeInputOutputFromHPC(
            pid=pid
        )

    TextToModelFunctions.removeProcessFromSystem(
            pid=pid
        )
