from typing import Annotated

from fastapi import APIRouter, Depends, Form, status
from sqlalchemy.orm import Session

from model.process.ProcessInfo import ProcessInfo
from model.process.StableDiffusionFunctions import StableDiffusionFunctions
from model.schema.textToImage_dataContainer import DataContainerAPI
from security.auth import get_current_user, get_db


# ------------------------------ general ------------------------------
CURRENT_MODEL = "0000-model-stableDiffusion"   # HPC foldername
ROUTE_NAME = 'stableDiffusion'                 # API general route


# ------------------------- General Route -----------------------------
router = APIRouter(
    prefix=f'/model/{ROUTE_NAME}',
    tags=[ROUTE_NAME]
)

# ---------------------------- Dependencies-----------------------------
db_dependency = Annotated[Session, Depends(get_db)]
user_dependency = Annotated[dict, Depends(get_current_user)]

# ------------------------------ Routes --------------------------------

# ------------------------------------------------------------------------
# ----------------------------- upload prompt ----------------------------
# ------------------------------------------------------------------------
@router.post("/upload_prompt", status_code=status.HTTP_200_OK)
async def upload_prompt(user: user_dependency, db: db_dependency, prompt: str = Form(...)):

    # register new process to system
    pid = StableDiffusionFunctions.createNewProcess(
        modelDataSchema=DataContainerAPI,
        prompt=prompt,
        usedModel=CURRENT_MODEL
    )

    return {'pid': pid}



# ------------------------------------------------------------------------
# ---------------------------- generate image ----------------------------
# ------------------------------------------------------------------------
@router.post("/generate_images", status_code=status.HTTP_200_OK)
async def generateImages(user: user_dependency, db: db_dependency, pid: str = Form(...), image_count: int = Form(...)):

    # start image job
    StableDiffusionFunctions.generateImages(
            pid=pid,
            image_count=image_count
        )



# ------------------------------------------------------------------------
# ----------------------------- status of run ----------------------------
# ------------------------------------------------------------------------
@router.post("/status_images", status_code=status.HTTP_200_OK)
async def status_model(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # check status of 3d job
    StableDiffusionFunctions.updateStatusStableDiffusionModel(
            pid=pid
        )

    start_time = ProcessInfo.processData[pid].startTime
    job_status = ProcessInfo.processData[pid].model_status.name

    return {
            'status': job_status,
            'startTime': start_time
        }



# ------------------------------------------------------------------------
# ---------------------------- download images ---------------------------
# ------------------------------------------------------------------------
@router.post("/get_images", status_code=status.HTTP_200_OK)
async def download_images(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    # get generated images
    binaryImageList = StableDiffusionFunctions.getImages(
            pid=pid
        )



    # convert the binary data to base64 encoded strings
    base64ImageList = StableDiffusionFunctions.convertBinaryListToBase64(binaryImageList)

    # return imageList as JSON-Object
    return {"images": base64ImageList}



# ------------------------------------------------------------------------
# ------------------------------- clean up ------------------------------
# ------------------------------------------------------------------------
@router.post("/clean_up", status_code=status.HTTP_200_OK)
async def clean_up(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    """
    Remove Input and Output files based on pid from HPC
    """

    StableDiffusionFunctions.removeInputOutputFromHPC(
            pid=pid
        )



# ------------------------------------------------------------------------
# ------------------------------ delete run ------------------------------
# ------------------------------------------------------------------------
@router.post("/delete_run", status_code=status.HTTP_200_OK)
async def delete_run(user: user_dependency, db: db_dependency, pid: str = Form(...)):
    """
    Remove process id from system
    """

    StableDiffusionFunctions.removeProcessFromSystem(
            pid=pid
        )



# ------------------------------------------------------------------------
# --------------- remove hpc content and api process ---------------------
# ------------------------------------------------------------------------
@router.post("/cleanAndDelete", status_code=status.HTTP_200_OK)
async def clean_delete_run(user: user_dependency, db: db_dependency, pid: str = Form(...)):

    StableDiffusionFunctions.removeInputOutputFromHPC(
            pid=pid
        )

    StableDiffusionFunctions.removeProcessFromSystem(
            pid=pid
        )
