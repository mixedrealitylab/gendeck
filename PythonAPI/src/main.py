from fastapi.middleware.httpsredirect import HTTPSRedirectMiddleware
from fastapi.middleware.trustedhost import TrustedHostMiddleware
from security.auth import get_current_user, get_db
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse
from fastapi.responses import RedirectResponse
from helper.json_reader import read_json_file
from fastapi import FastAPI, status, Depends
from fastapi.staticfiles import StaticFiles
from starlette.responses import Response

import model.one2345 as model_one2345
import model.wonder3d as model_winder3d
import model.eShape_textTo3D as model_eShape_textTo3D
import model.stableDiffusion as model_stableDiffusion

from sqlalchemy.orm import Session
from data.database import engine
import model.demo as model_demo
import security.auth as auth
import data.models as models
from typing import Annotated
import logging
import ssl
import os

logging.basicConfig(level=logging.INFO, format="%(levelname)s:\t\t%(message)s")

app = FastAPI()

# ------------- Middleware -------------------------------
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# ------------- Routes -------------------------------
app.include_router(auth.router)
app.include_router(model_demo.router)
app.include_router(model_one2345.router)
app.include_router(model_winder3d.router)
app.include_router(model_eShape_textTo3D.router)
app.include_router(model_stableDiffusion.router)

models.Base.metadata.create_all(bind=engine)

db_dependency = Annotated[Session, Depends(get_db)]
user_dependency = Annotated[dict, Depends(get_current_user)]


# ------------- Check user authentication ----------------
@app.get("/version", status_code=status.HTTP_200_OK)
async def version():
    return read_json_file("data/static_information/version_info.json")
