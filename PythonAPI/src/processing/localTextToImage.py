from time import sleep
from fastapi import HTTPException, status
from helper.status import Status
from helper.sd_model import StableDiffusionModel
from model.process.ProcessInfo import ProcessInfo
from model.schema.textToImage_dataContainer import DataContainerAPI
from model.process.StableDiffusionFunctions import StableDiffusionFunctions
import model.stableDiffusion as StableDiffusion
import requests
import logging
import io

WAIT_TIME = 1
MAX_REQUESTS = 20

async def text_to_images(prompt: str, count: int=1, stableDiffusionModel: StableDiffusionModel = StableDiffusionModel.SD_1_5) -> list[io.BytesIO]:

    current_request_count: int = 0

    # set prompt
    pid = StableDiffusionFunctions.createNewProcess(
        modelDataSchema=DataContainerAPI,
        prompt=prompt,
        usedModel="0000-model-stableDiffusion"
    )

    # run model
    match stableDiffusionModel:
        case StableDiffusionModel.SD_1_5:
            StableDiffusionFunctions.generateImages(
                pid=pid,
                image_count=count
            )
        case _:
            StableDiffusionFunctions.generateImages(
                pid=pid,
                image_count=count
            )

    # wait for result
    status: Status = Status.RUNNING

    while(status != Status.WAIT_FOR_CLEANING or current_request_count != MAX_REQUESTS):

        sleep(WAIT_TIME)

        # add next request to count
        current_request_count += 1

        # update status
        StableDiffusionFunctions.updateStatusStableDiffusionModel(
            pid=pid
        )

        status = ProcessInfo.processData[pid].model_status.name

        # model is finished
        if(status == Status.WAIT_FOR_CLEANING.name):

            # download images
            binaryImageList = StableDiffusionFunctions.getImages(
                pid=pid
            )

            # remove images from HPC
            StableDiffusionFunctions.removeInputOutputFromHPC(
                pid=pid
            )

            StableDiffusionFunctions.removeProcessFromSystem(
                pid=pid
            )

            # return result
            return binaryImageList
        else:
            continue

    return []


