from logging import Logger
import paramiko as pm
from PIL import Image
import tempfile
import io
import os



class HPC_Connector():

    @staticmethod
    def get_ssh_path():

        home_directory = os.path.expanduser("~")
        ssh_path = os.path.join(home_directory, '.ssh')

        return ssh_path



    @staticmethod
    def create_connection_by_private_key(hostname: str, jumpName: str, private_key_path: str, hosts_path: str = None):

        # get private key for ssh connection
        private_key = pm.RSAKey(filename=private_key_path)

        jumpBox = pm.SSHClient()
        jumpBox.set_missing_host_key_policy(pm.AutoAddPolicy())

        # load ssh conf
        config = pm.SSHConfig()
        try:
            config.parse(open(hosts_path))
        except IOError:
            pass

        jump_config = config.lookup(jumpName)
        host_config = config.lookup(hostname)

        # create connection
        jumpBox.connect(hostname=jump_config['hostname'],
                       username=jump_config['user'],
                       key_filename=private_key_path,
                       look_for_keys=False)

        jumpBox_channel = jumpBox.get_transport().open_channel(
            'direct-tcpip', (host_config['hostname'], 22), ('', 0)
        )

        client = pm.SSHClient()
        client.set_missing_host_key_policy(pm.AutoAddPolicy())

        # create connection
        client.connect(hostname=host_config['hostname'],
                       username=host_config['user'],
                       key_filename=private_key_path,
                       sock=jumpBox_channel,
                       look_for_keys=False)

        return client, host_config


    @staticmethod
    def execute_command(ssh_client: pm.SSHClient, command: str, current_directory: str) -> (str, str, str):
        """ Send command to HPC

        Args:
            ssh_client (pm.SSHClient)
            command (_type_)

        Returns:
            command, output, error: (str, str, str)
        """

        # execute command
        stdin, stdout, stderr = ssh_client.exec_command(f"cd {current_directory}; {command}")

        output = stdout.read().decode()
        error = stderr.read().decode()

        return (command, output, error)


    @staticmethod
    def upload_file_content(ssh_client: pm.SSHClient, remote_path: str, file_content) -> bool:

        try:
            # write content in temporary file
            temp_local_path = tempfile.NamedTemporaryFile(delete=False).name
            with open(temp_local_path, 'wb') as temp_file:
                temp_file.write(file_content.getbuffer())
        except Exception as e:
            print(f"An error occurred: {e}")
            return False

        try:
            # create sftp connection
            sftp = ssh_client.open_sftp()

            # upload file
            sftp.put(temp_local_path, remote_path)

            # close connection
            sftp.close()

            # delete temp file
            os.unlink(temp_local_path)

            return True

        except FileNotFoundError as e:
            print(f"Local file not found: {temp_local_path}")
        except PermissionError as e:
            print(f"Permission error: {e}")
        except Exception as e:
            print(f"An error occurred: {e}")
        return False



    @staticmethod
    def upload_image_content(ssh_client: pm.SSHClient, remote_path: str, image_content : bytes) -> bool:

        try:
            # write content in temporary file
            image_io = io.BytesIO(image_content)

            # Open the image using PIL
            image = Image.open(image_io)

            # Create a temporary file
            with tempfile.NamedTemporaryFile(delete=False, suffix='.png') as temp_file:

                # Save the image content to the temporary file
                image.save(temp_file, format='PNG')

                temp_local_path = temp_file.name

        except Exception as e:
            print(f"An error occurred: {e}")
            return False

        try:
            # create sftp connection
            sftp = ssh_client.open_sftp()

            # upload file
            sftp.put(temp_local_path, remote_path)

            # close connection
            sftp.close()

            # delete temp file
            os.unlink(temp_local_path)

            return True

        except FileNotFoundError as e:
            print(f"Local file not found: {temp_local_path}")
        except PermissionError as e:
            print(f"Permission error: {e}")
        except Exception as e:
            print(f"An error occurred: {e}")
        return False



    @staticmethod
    def upload_file(ssh_client: pm.SSHClient, remote_path: str, local_path: str) -> bool:

        if not os.path.isfile(local_path):
            print('Could not find localFile %s !!' % local_path)
            return False

        try:
            # upload to hpc
            sftp = ssh_client.open_sftp()
            sftp.logger.level = 2
            files = sftp.listdir()
            current_directory = sftp.getcwd()
            print(current_directory)
            print(files)

            print(f"Uploading local file: {local_path}")
            print(f"To remote path: {remote_path}")
            sftp.put(local_path, remote_path)
            print("Upload successful")
            sftp.close()

            return True
        except FileNotFoundError as e:
            print(f"Local file not found: {local_path}")
        except PermissionError as e:
            print(f"Permission error: {e}")
        except Exception as e:
            print(f"An error occurred: {e}")
        return False



    @staticmethod
    def download_file(ssh_client: pm.SSHClient, remote_path: str, encoding: bool = True):

        try:
            # download file form hpc
            sftp = ssh_client.open_sftp()
            with sftp.file(remote_path, 'r') as remote_file:
                file_content = remote_file.read()

                if(encoding == True):
                    file_content = file_content.decode()
            sftp.close()

            return file_content
        except Exception as e:
            print(e)
            return



    @staticmethod
    def is_ssh_connection_open(ssh_client: pm.SSHClient) -> bool:

        # get transport layer
        transport = ssh_client.get_transport()

        return transport.is_active()
