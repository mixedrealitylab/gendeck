from fastapi import HTTPException, status
import requests
import logging
import trimesh
import time
import io


URL = "https://photo-2-3d.beal2o4.moe"


def uploadImage(image_file: io.BytesIO):
    
    image_data = {"image": image_file}
    
    api_url = f"{URL}/submit"
    response = requests.post(api_url, files=image_data)
    
    if response.status_code == 200:
        response_data = response.json()
        
        id = response_data.get("id")
        logging.info(f"uploaded image - ID: {id}.")
        return id
    else:
        logging.error("error upload image")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Upload ImageToObject Failed")
        
        
def checkStatus(id):
    api_url = f"{URL}/model/{id}/status"
    response = requests.get(api_url)
    
    if response.status_code == 200:
        response_data = response.json()
        
        id = response_data.get("id")
        readyStatus = response_data.get("ready")
        
        logging.info(f"check model status - finished: {readyStatus}, ID: {id}")
        return readyStatus
    else:
        logging.error("error check status")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="CheckStatus ImageToObject Failed")
    
    
def downloadObj(id, filepath='output.obj'):
    api_url = f"{URL}/model/{id}"
    response = requests.get(api_url)

    if response.status_code == 200:
        logging.info("download object")
        return trimesh.load(file_obj=io.BytesIO(response.content), file_type="obj")
    
    else:
        logging.error("error download object")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Download ImageToObject Failed")
    
    
def checkRunSeconds(start_time, seconds):
    end_time = time.time()

    elapsed_time = end_time - start_time
    return elapsed_time > seconds


def sleeping(seconds):
    run = False
    start_time = time.time()
    while(run == False):
        if(checkRunSeconds(start_time, seconds)):
            run = True