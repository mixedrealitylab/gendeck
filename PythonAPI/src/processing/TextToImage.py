import base64
from fastapi import HTTPException, status
import requests
import logging
import io

TOKEN = "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"
HEADERS = {"Authorization": f"Bearer {TOKEN}"}
ENDPOINT = "https://api-inference.huggingface.co/models/"
MAX_FAIL = 10

modelList = [
    "runwayml/stable-diffusion-v1-5",
    "stabilityai/stable-diffusion-2-1"
]


def text_to_image(prompt: str, count: int) -> io.BytesIO:

    global modelList
    global currentModelIdx

    failCount = 0

    payload = {
        "inputs": prompt
        }





    # check for error
    for current_hf_model in modelList:

        binaryImageList = []
        fail_count = 0

        # define url
        request_point = ENDPOINT + current_hf_model

        while fail_count < MAX_FAIL:

            # multiple images
            for _ in range(count):

                binaryImg = getHuggingFaceResult(request_point, HEADERS, payload)

                # check response
                if(binaryImg != None):
                    # return list of one binary images
                    binaryImageList.append(binaryImg)
                else:
                    logging.error(f"error by using HuggungFace model: {current_hf_model}")
                    failCount += 1
                    break

                if len(binaryImageList) >= count:
                    return binaryImageList
    return []


def getHuggingFaceResult(request_point: str, headers, payload):

    # wait fo response
    response = requests.post(request_point, headers=HEADERS, json=payload)

    # check response
    if(response.status_code == 200):
        binaryImg = response.content

        logging.info("success generating with HuggingFace an image")

        return binaryImg

    else:
        logging.error(f"error by using HuggungFace model: {request_point}")

        return None