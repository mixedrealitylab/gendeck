from model.process.ProcessInfo import ProcessInfo
from processing.HPC.Connection import HPC_Connector
from decouple import config
import logging
import time
import os

from PIL import Image
import tempfile
import io


class ModelProcessing:


    # Config
    HOSTNAME = "alex"
    JUMPNAME = "cshpc"
    KEY_PATH = config("keyPath").replace("\\", "/")
    KNOWN_HOSTS = config("knownHosts").replace("\\", "/")
    HPC_VAULT = config("hpcPath")
    HPC_MODEL_FOLDER = config("hpcModelFolder")

    current_job_count = 0

    # connection variables
    client = None
    host_config = None

    @staticmethod
    def checkConnection():
        if ModelProcessing.client == None or not HPC_Connector.is_ssh_connection_open(ModelProcessing.client):
            ModelProcessing.client, ModelProcessing.host_config = HPC_Connector.create_connection_by_private_key(ModelProcessing.HOSTNAME, ModelProcessing.JUMPNAME, ModelProcessing.KEY_PATH, ModelProcessing.KNOWN_HOSTS)


    @staticmethod
    def copy_image_content(image_content, model_name, pid):

        # check for connection
        ModelProcessing.checkConnection()

        # get image id for identification

        remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/input/{pid}.png"
        upload_status = HPC_Connector.upload_image_content(ModelProcessing.client, image_content=image_content, remote_path=remote_path)

        return upload_status


    @staticmethod
    def copy_text_content(text_content, model_name, pid):

        # check for connection
        ModelProcessing.checkConnection()

        # get image id for identification

        remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/input/{pid}.txt"
        upload_status = HPC_Connector.upload_file_content(ModelProcessing.client, file_content=text_content, remote_path=remote_path)

        return upload_status


    @staticmethod
    def remove_image_content(pid, model_name):

        # check for connection
        ModelProcessing.checkConnection()

        remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/input"
        (command, output, error) = HPC_Connector.execute_command(ssh_client=ModelProcessing.client, current_directory=remote_path, command=f"rm {pid}.png")

        if error != '':
            logging.error("remove image content error: " + error)


    @staticmethod
    def start_job(pid : str,
                    model_name : str,
                    input_file_extension: str = "png",
                    parameters: list = []):

        # check for connection
        ModelProcessing.checkConnection()

        jobName = f"{model_name}-procNr-{ModelProcessing.current_job_count}"

        start_command = f"sbatch --job-name={jobName} ./job.sh"

        func_chars = ["-"]

        # define input file
        if(input_file_extension != "" and input_file_extension not in func_chars):
            start_command += f" {pid}.{input_file_extension}"

        if(input_file_extension == func_chars[0]):
            start_command += f" {pid}"

        # add parameter
        for parameter in parameters:
            start_command += f" {parameter}"

        remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}"

        (command, output, error) = HPC_Connector.execute_command(ssh_client=ModelProcessing.client, current_directory=remote_path, command=start_command)

        if error != '':
            raise Exception(f"Can not start Job: {error}")

        job_id = output.strip().split()[-1]

        ModelProcessing.current_job_count += 1

        return job_id


    @staticmethod
    def check_job(job_id) -> bool:

        command = f"squeue -o '%i %j' | grep {job_id}"

        remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation"

        # remove model
        try:
            (command, output, error) = HPC_Connector.execute_command(ssh_client=ModelProcessing.client, current_directory=remote_path, command=command)
        except:
            print(f"can not check job: {error}")

        # job is still running
        if output != '':
            return True

        return False


    @staticmethod
    def convert_obj_file(pid, model_name, obj_name: str, convert_extension = ".dae"):

        job_id = ProcessInfo.processData[pid].job_id

        # check if job is finished
        if(ModelProcessing.check_job(job_id=job_id) == False):

            logging.info("convert obj model")

            scriptFolder = "helper"
            scriptName = "runObjToDoe"

            script_dir = ModelProcessing.HPC_VAULT + f"0-model-calculation/{scriptFolder}"
            script_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{scriptFolder}/{scriptName}.sh"


            remote_path_obj = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/output/{pid}/{obj_name}"
            remote_path_extension = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/output/{pid}/{obj_name.replace('.obj', convert_extension)}"

            # create subshell to run module
            command = f"(sbatch {script_path} {remote_path_obj} {remote_path_extension})"

            try:
                (command, output, error) = HPC_Connector.execute_command(ssh_client=ModelProcessing.client, current_directory=script_dir, command=command)
            except:
                raise Exception(f"can not convert .obj file: {error}")

            return remote_path_extension


    @staticmethod
    def check_file_exist(filePath: str):

        scriptFolder = "helper"
        scriptName = "checkFileExists"

        script_dir = ModelProcessing.HPC_VAULT + f"0-model-calculation/{scriptFolder}"
        script_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{scriptFolder}/{scriptName}.sh"

        command = f"( source {script_path} {filePath})"

        try:
            (command, output, error) = HPC_Connector.execute_command(ssh_client=ModelProcessing.client, current_directory=script_dir, command=command)
        except:
            print(f"can not check file existents: {error}")

        if "false" in output:
            return False
        else:
            return True


    @staticmethod
    def download_object(pid, model_name, obj_name: str):

        job_id = ProcessInfo.processData[pid].job_id

        # check if job is finished
        if(ModelProcessing.check_job(job_id=job_id) == False):

            logging.info("load 3d model")

            remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/output/{pid}/{obj_name}"

            download_content = HPC_Connector.download_file(ssh_client=ModelProcessing.client, remote_path=remote_path)

            return download_content


    @staticmethod
    def download_image(pid, model_name, image_name):

        job_id = ProcessInfo.processData[pid].job_id

        # check if job is finished
        if(ModelProcessing.check_job(job_id=job_id) == False):

            logging.info("load 2d image")

            remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/output/{pid}/{image_name}"

            download_content = HPC_Connector.download_file(ssh_client=ModelProcessing.client, remote_path=remote_path, encoding=False)

            return download_content


    @staticmethod
    def remove_object_content(pid, model_name):

        # check for connection
        ModelProcessing.checkConnection()

        # remove model
        remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}/output"

        (command, output, error) = HPC_Connector.execute_command(ssh_client=ModelProcessing.client, current_directory=remote_path, command=f"rm -rf {pid}")

        if error != '':
            logging.error("remove results error: " + error)

        # remove log
        job_id = ProcessInfo.processData[pid].job_id
        remote_path = ModelProcessing.HPC_VAULT + f"0-model-calculation/{model_name}"
        command = f"rm *{job_id}"

        (command, output, error) = HPC_Connector.execute_command(ssh_client=ModelProcessing.client, current_directory=remote_path, command=command)

        if error != '':
            logging.error("remove log error: " + error)



    @staticmethod
    def add_comment_to_obj_content(content: str, model_name: str):

        if(content == None):
            return content

        comment = f"# created with model: " + model_name + "\n"

        return comment + content