import json
from time import sleep
import pytest
import requests

TEST_USERNAME = "user"
TEST_PASSWORD = "password"
TEST_URL = "127.0.0.1"
TEST_PORT = "8000"
TEST_MODEL = "stableDiffusion"

valid_test_pid = ""

@pytest.fixture
def base_url():
    # define testing url
    return f"http://{TEST_URL}:{TEST_PORT}"

@pytest.fixture
def get_token(base_url):
    # define route
    AUTH_ROUTE = "auth/token"
    login_url = f"{base_url}/{AUTH_ROUTE}"

    # define username and password
    credentials = {"username": TEST_USERNAME, "password": TEST_PASSWORD}

    # get token
    response = requests.post(login_url, data=credentials)

    # check token
    if response.status_code == 200:
        return response.json()["access_token"]
    else:
        raise ValueError("Failed to obtain token")

# helper
def extractPidOfResponse(responseContent):
    # convert bytes to string and parse JSON
    response_json = responseContent.decode('utf-8')
    response_dict = json.loads(response_json)

    # extract and return the PID value
    return response_dict.get("pid")

def isStatusFinished(responseContent):
    # convert bytes to string and parse JSON
    response_json = responseContent.decode('utf-8')
    response_dict = json.loads(response_json)

    # extract status
    status = response_dict.get("status")

    if(status != "WAIT_FOR_CLEANING"): return False
    else: return True

# -------- Test positive upload_prompt --------
def test_upload_positive_prompt(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": "cookie"}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 200

    pid = extractPidOfResponse(responseContent=response.content)
    valid_test_pid = pid

# -------- Test negative upload_prompt --------
def test_uploadPrompt_negative_prompt_empty_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": ""}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 422

def test_uploadPrompt_negative_prompt_without_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": ""}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 422

def test_uploadPrompt_negative_prompt_invalid_content_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": " "}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_uploadPrompt_negative_prompt_invalid_length_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": "a"}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test positive generate_images --------
def test_generateImages_positive_prompt_one_image(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/generate_images"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid,
        "image_count": 1
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 200

def test_generateImages_positive_prompt_multiple_image(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/generate_images"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid,
        "image_count": 5
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 200

# -------- Test negative get_images --------
def test_getImages_negative_imageGenerationNotFinished(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/generate_images"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 422

# -------- Test positive status_images --------
def test_statusImages_negative_imageGenerationNotFinished(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/status_images"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 200

# -------- Test positive get_images --------
def test_getImages_negative_imageGenerationNotFinished(base_url, get_token):
    global valid_test_pid

    finishedGen : bool = False      # isFinished gen
    MAX_WAIT: int = 60              # seconds
    WAIT_TIME: int = 2
    wait_counter = 0

    test_upload_positive_prompt(base_url, get_token)
    test_generateImages_positive_prompt_one_image(base_url, get_token)

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/status_images"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    while(finishedGen == False):
        # set request
        response = requests.post(url, headers=headers, data=data)

        # check if image generation is finished
        finishedGen = isStatusFinished(responseContent=response.content)

        sleep(WAIT_TIME)

        wait_counter += 1

        if(MAX_WAIT < wait_counter): break
        if(finishedGen == True): break

    if(finishedGen == False):
        assert "generation takes to long"

    # download images
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/get_images"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    response = requests.post(url, headers=headers, data=data)

    assert response.status_code == 200


