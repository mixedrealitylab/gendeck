import json
from time import sleep
import pytest
import requests

TEST_USERNAME = "user"
TEST_PASSWORD = "password"
TEST_URL = "127.0.0.1"
TEST_PORT = "8000"
TEST_MODEL = "wonder3d"

valid_test_pid = ""

@pytest.fixture
def base_url():
    # define testing url
    return f"http://{TEST_URL}:{TEST_PORT}"

@pytest.fixture
def get_token(base_url):
    # define route
    AUTH_ROUTE = "auth/token"
    login_url = f"{base_url}/{AUTH_ROUTE}"

    # define username and password
    credentials = {"username": TEST_USERNAME, "password": TEST_PASSWORD}

    # get token
    response = requests.post(login_url, data=credentials)

    # check token
    if response.status_code == 200:
        return response.json()["access_token"]
    else:
        raise ValueError("Failed to obtain token")

# helper
def extractPidOfResponse(response):
    # Convert bytes to string and parse JSON
    response_json = response.decode('utf-8')
    response_dict = json.loads(response_json)

    # Extract and return the PID value
    return response_dict.get("pid")

# -------- Test positive upload_prompt --------
def test_upload_positive_prompt(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": "cookie"}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 200

    pid = extractPidOfResponse(response=response.content)
    valid_test_pid = pid

# -------- Test negative upload_prompt --------
def test_uploadPrompt_negative_prompt_empty_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": ""}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 422

def test_uploadPrompt_negative_prompt_without_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": ""}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 422

def test_uploadPrompt_negative_prompt_invalid_content_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": " "}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_uploadPrompt_negative_prompt_invalid_length_prompt(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/upload_prompt"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"prompt": "a"}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test negative get_generated_image --------
def test_getGeneratedImage_negative_no_images_generated_get_single_image_without_valid_pid(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/get_generated_image"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": "abc",
        "getImageNr": 1
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_getGeneratedImage_negative_no_images_generated_get_single_image(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/get_generated_image"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid,
        "getImageNr": 1
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_getGeneratedImage_negative_no_images_generated_get_all_images(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/get_generated_image"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid,
        "getImageNr": 1
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test negative select_image --------
def test_selectImage_negative_no_images_generated_select_single_image_without_valid_pid(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/select_image"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": "abc",
        "useImageNr": 1
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_selectImage_negative_no_images_generated_select_single_image(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/select_image"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid,
        "useImageNr": 1
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test negative generate_model --------
def test_generateModel_negative_no_images_generated_without_valid_pid(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/generate_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": "abc",
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_generateModel_negative_no_images_generated(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/generate_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test negative download_model --------
def test_downloadModel_negative_no_images_generated__without_valid_pid(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/download_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": "abc",
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_downloadModel_negative_no_images_generated(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/download_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test negative status_model --------
def test_statusModel_negative_no_images_generated_without_valid_pid(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/status_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": "abc",
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

def test_statusModel_negative_no_images_generated(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/status_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test negative clean_up --------
# failed on pid
def test_cleanUp_negative_no_images_generated_without_valid_pid(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/clean_up"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": "abc",
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# failed on job_id - to delete run description
def test_cleanUp_negative_no_images_generated(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/clean_up"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test negative delete_run --------
# failed on pid
def test_deleteRun_negative_no_images_generated_without_valid_pid(base_url, get_token):
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/delete_run"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": "abc",
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 400

# -------- Test positive delete_run --------
def test_deleteRun_negative_no_images_generated(base_url, get_token):
    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/delete_run"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 200

# -------- Test positive generate_image
def test_generateImage_positive(base_url, get_token):

    test_upload_positive_prompt(base_url, get_token)

    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/generate_image"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {"pid": valid_test_pid}

    # set request
    response = requests.post(url, headers=headers, data=data)

    # check result
    assert response.status_code == 200


# -------- Test complete run --------
def test_complete_run_one_image(base_url, get_token):

    finishedGen : bool = False      # isFinished gen
    MAX_WAIT: int = 360              # seconds
    WAIT_TIME: int = 10
    wait_counter = 0

    test_generateImage_positive(base_url, get_token)

    global valid_test_pid

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/generate_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

    assert response.status_code == 200

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/status_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    while(finishedGen == False):
        # set request
        response = requests.post(url, headers=headers, data=data)

        # check if image generation is finished
        finishedGen = isStatusFinished(responseContent=response.content)

        sleep(WAIT_TIME)

        wait_counter += 1

        if(MAX_WAIT < wait_counter): break
        if(finishedGen == True): break

    if(finishedGen == False):
        assert "generation takes to long"

    # download images
    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/download_model"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    response = requests.post(url, headers=headers, data=data)

    assert response.status_code == 200

    # configuration
    url = f"{base_url}/model/{TEST_MODEL}/cleanAndDelete"
    headers = {"Authorization": f"Bearer {get_token}"}

    # set data
    data = {
        "pid": valid_test_pid
    }

    # set request
    response = requests.post(url, headers=headers, data=data)

def isStatusFinished(responseContent):
    # convert bytes to string and parse JSON
    response_json = responseContent.decode('utf-8')
    response_dict = json.loads(response_json)

    # extract status
    status = response_dict.get("status")

    if(status != "WAIT_FOR_CLEANING"): return False
    else: return True