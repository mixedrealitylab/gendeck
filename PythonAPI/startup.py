from pathlib import Path
import subprocess
import os

# parameter
ssh_key_name = "rsa_key"
storage_folder = "config_data"


# ---- user information ---
def get_user_input(parameter_name):
    return input(f"Enter {parameter_name}: ")



# ------- .env -------------
def write_env_file():

    print(f"2/4 start: create .env ")

    # API
    secret = get_user_input("application secret")
    if secret == "": secret = "xabrispjgieyx2d9hhobsjijp5woo6uc3wtm2uo4g6u4vyt8aw9vp38pwboomeqykhq7v8thx3b2gwekny9bz27x8vaxnheswbkga2kh5n54mzjx99usmxw4ur"

    algorithm = get_user_input("signing algorithm")
    if algorithm == "": algorithm = "HS256"

    time = get_user_input("expire time token")
    if time == "": time = "300"

    jobLimit = get_user_input("API job limit")
    if jobLimit == "": jobLimit = "3"


    # Huggingface
    huggingfaceToken = get_user_input("huggingface token")
    if huggingfaceToken == "": huggingfaceToken = ""

    huggingfaceMaxRequestFails = get_user_input("huggingface max request fails")
    if huggingfaceMaxRequestFails == "": huggingfaceMaxRequestFails = "10"

    # HPC
    vaultPath = get_user_input("HPC alex vault path (/home/vault/<group>/<user>/...)")
    if vaultPath == "": vaultPath = ""

    modelFolder = get_user_input("HPC model folder (default: 0-model-calculation)")
    if modelFolder == "": modelFolder = "0-model-calculation"

    hpcGroupName = get_user_input("HPC group name")
    if hpcGroupName == "": hpcGroupName = "b116ba"

    hpcUserName = get_user_input("HPC user name")
    if hpcUserName == "": hpcUserName = ""

    # check vault path
    if vaultPath != "" and not vaultPath.endswith('/'):
        vaultPath += '/'

    # Create content for the .env file
    env_content = f"""secret = {secret}
algorithm = {algorithm}
time = {time}
jobLimit = {jobLimit}
huggingfaceToken = {huggingfaceToken}
huggingfaceMaxRequestFail = {huggingfaceMaxRequestFails}
keyPath = {os.getcwd()}/{storage_folder}/{ssh_key_name}
knownHosts = {os.getcwd()}/{storage_folder}/ssh_config
hpcPath = /home/vault/{hpcGroupName}/{hpcUserName}/{vaultPath}
hpcModelFolder = {modelFolder}"""

    # Write content to .env file
    env_file_path = ".env"
    with open(env_file_path, "w") as env_file:
        env_file.write(env_content)

    print(f"2/4 end: create .env")

    return hpcUserName



# ------- rsa key ----------
def generate_ssh_key(directory="./data"):
    print(f"1/4 start: create ssh key")

    # Ensure the directory exists
    os.makedirs(directory, exist_ok=True)

    keyFilePath = os.path.join(directory, ssh_key_name)
    public_key_path = f"{keyFilePath}"

    command = [
        "ssh-keygen",
        "-t", "rsa",
        "-b", "4096",
        "-C", "speechTo3D",
        "-f", keyFilePath,
        "-N", ""
    ]

    subprocess.run(command)

    print(f"1/4 end: create ssh key")

    return os.path.join(os.getcwd(), public_key_path)



# ------- ssh_config -------
def get_cshpc_config(userName, identityFilePath):
    config = "Host cshpc\n"
    config += "    HostName cshpc.rrze.fau.de\n"
    config += f"    User {userName}\n"
    config += f"    IdentityFile {identityFilePath}\n"
    config += "    IdentitiesOnly yes\n"
    config += "    PasswordAuthentication no\n"
    config += "    PreferredAuthentications publickey\n"

    return config

def get_alex_config(userName, identityFilePath):
    config = "Host alex\n"
    config += "    HostName alex.nhr.fau.de\n"
    config += f"    User {userName}\n"
    config += "    ProxyJump cshpc\n"
    config += f"    IdentityFile {identityFilePath}\n"
    config += "    IdentitiesOnly yes\n"
    config += "    PasswordAuthentication no\n"
    config += "    PreferredAuthentications publickey\n"

    return config

def write_ssh_config(user, identityFilePath, path):

    print(f"3/4 start: create ssh_config")

    # get config data
    config_cshpc = get_cshpc_config(user, identityFilePath)
    config_alex = get_alex_config(user, identityFilePath)

    # combine config data
    config = config_cshpc + "\n" + config_alex

    # save config data
    with open(path +'/ssh_config', 'w') as file:
        file.write(config)

    print(f"3/4 end: create ssh_config")



# ------- setup python ------
def venv_exists():
    return Path(".venv").exists()

def create_virtual_environment():
    subprocess.run(["python", "-m", "venv", ".venv"], check=True)

def activate_virtual_environment():

    if os.name == 'posix':  # Unix or MacOS
        subprocess.run([r"./.venv/bin/pip", "install", "-r", "./src/requirements.txt"], check=True)

    elif os.name == 'nt':  # Windows
        subprocess.run([r"./.venv/Scripts/pip.exe", "install", "-r", "./src/requirements.txt"], check=True)

def setup_python():

    print(f"4/4 start: setup python env.")

    if(not venv_exists()):
        create_virtual_environment()
        activate_virtual_environment()
    else:
        activate_virtual_environment()
        print("4/4 status: python env. already exist")

    print(f"4/4 end: setup python env.")



def main():

    # generate ssh key for hpc
    keyFolder = generate_ssh_key(storage_folder)

    # generate .env
    userName = write_env_file()

    # generate ssh_config
    write_ssh_config(user=userName, identityFilePath=keyFolder, path=storage_folder)

    # setup python environment
    setup_python()


if __name__ == '__main__':
    main()

    ip = "0.0.0.0"
    print("--- Finished ---")
    print(f"activate python env. windows: '.\\.venv\\Scripts\\activate' or linux: 'activate .venv/bin/activate'")
    print("use 'cd src'")
    print("get your ip address: windows-cmd 'ipconfig', linux-shell: 'ip a'")
    print(f"use 'uvicorn main:app --host {ip} --port 8000' to start the API")
    print(f"open swagger overview with http://localhost:8000/docs")